/**
 * 
 */
package util;

import java.awt.LayoutManager;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public abstract class PanelNew extends JPanel{
	
	public PanelNew(Rectangle bounds, LayoutManager layout){
		setBounds(bounds);
		setLayout(layout);
	}
	
	public void hidePanel(){
		setVisible(false);
	}
	
	public void showPanel(){
		setVisible(true);
	}
	
	public abstract void initComponents();
	
	public abstract void addComponents();
}
