/**
 * 
 */
package util;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public abstract class Frame extends JFrame implements MouseListener{
	public Frame(String name){
        super(name);
        addMouseListener(this);
	}
	
	public void init(){
	        loadImages();
	        initComponents();
	        addComponents();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("\nMouse at: "+e.getX()+","+e.getY());
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
	
	public void showFrame(boolean show){
	    setVisible(show);
	}
	public abstract void loadImages();
	public abstract void initComponents();
	public abstract void addComponents();
}
