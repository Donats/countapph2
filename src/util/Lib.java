/**
 * 
 */
package util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import model.BallotData;
import model.BallotType;
import model.CandidateFile;
import model.CandidateName;
import view.MainFrame;

/**
 * @author donatoamasa
 *
 */
public class Lib {

	public static final String 
		OPEN_BALLOT = "Ballot file(s) successfully loaded.",
		CLUSTER_IMAGE = "Initiating ballots and cluster method.",
		CLUSTER_DONE = "Ballots successfully clustered. New ballot template(s) created.",
		CHECK_BALLOT = "Checking the ballots..",
		CHECK_DONE = "Successfully checked and counted the ballots!",
		SAVE_TEMPLATE = "Template successfully saved!";
	
	public static final String [] POSITION_TOWNS = {"12 Senator", 
												"1 Member, House of Representatives",
												"1 Provincial Governor",
												"1 Provincial Vice-Governor",
												"1 Member, Sangguniang Panlalawigan",
												"1 Mayor",
												"1 Vice-Mayor",
												"1 Member, Sangguniang Panglungsod/Bayan"};
	public static final String [] POSITION_CITY_DISTRICT = {"12 Senator",
															"1 Member, House of Representatives",
															"1 Mayor",
															"1 Vice-Mayor",
															"1 Member, Sangguniang Panglungsod/Bayan"};
	
	public static String correctNames;
	
	public static void setFrame(JFrame frame, int LaF, int frameWidth, int frameHeight, LayoutManager layout, boolean isResizable, boolean isVisible, String iconImg) {
		UIManager.LookAndFeelInfo looks[] = UIManager.getInstalledLookAndFeels();
		try{
			UIManager.setLookAndFeel(looks[LaF].getClassName());
			SwingUtilities.updateComponentTreeUI(frame);
		} catch(Exception e){}
        if(iconImg != null)
            frame.setIconImage(new ImageIcon(iconImg).getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(frameWidth, frameHeight);
		frame.setLayout(layout);
		frame.setLocationRelativeTo(null);
		frame.setVisible(isVisible);
		frame.setResizable(isResizable);
	}
	
	/**
	 * creates a Rectangle Object
	 * @param x
	 * @param y
	 * @param framewidth
	 * @param frameheight
	 * @return a Rectangle using the parameters
	 */
	
	public static void loadOpenCV(){
		try {
	        String osName = System.getProperty("os.name");

	        if (osName.startsWith("Windows")) {
	            int bitness = Integer.parseInt(System.getProperty("sun.arch.data.model"));
	            if (bitness == 32) {
	                loadLibByNIO(86+"");
	            } else if (bitness == 64) {
	                loadLibByNIO(64+"");
	            } else {
	                loadLibByNIO(86+"");
	            }
	        }
	      
	    }catch (Exception e) {
	    	JOptionPane.showMessageDialog(MainFrame.getInstance(),
				"Error loading opencv, check location of opencv_java300.dll",
				"File error",
				JOptionPane.ERROR_MESSAGE);
        }
	}

	private static void loadLibByNIO(String bit){
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		System.load(s+"/opencv/bit/x"+bit+"/opencv_java320.dll");
	}
	
	public static void loadCorrectNames(){
		correctNames = readResult("images/correctNames.txt");
	}
	
	public static Rectangle createRect(int x, int y, int width, int height) {
		return new Rectangle(x,y,width,height);
	}

	public static Dimension createDimension(int width, int height) {
		return new Dimension(width, height);
	}

	public static Point createPoint(int x, int y) {
		return new Point(x,y);
	}
	
	public static BufferedImage createImage(URL filePath){
		BufferedImage img = null;
		try {
			img = ImageIO.read(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
	
	public static Font getDefaultFont(int fontSize){
		return new Font("Arial", Font.PLAIN, fontSize);
	}
	
	public static Color getPositionBackgroundColor(String position){
		return (position.equals("Senator") || position.equals("Provincial Governor") || position.equals("Member, Sangguniang Panlalawigan") || position.equals("Vice-Mayor")) ?  new Color(0,112,192) : new Color(0,176,80);
	}
	
	public static boolean checkMaxVotes(String position){
		return (position.equals("Member, Sangguniang Panlalawigan") || position.equals("Member, Sangguniang Panglungsod/Bayan") ? true : false);
	}
	
	public static Color getDefaultColor(){
		return new Color(60, 60, 60);
	}
	
	public static DefaultListCellRenderer getCellRenderer(){
		DefaultListCellRenderer cellRenderer = new DefaultListCellRenderer();
		cellRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER); 
		return cellRenderer;
	}
	
	public static void printMsg(Object str) {
		System.out.println(str);
	}
	
	public static void saveBallotTemplate(ArrayList<BallotType> ballotTypes, ArrayList<JTextField> textFieldList, ArrayList<JTextField> maxVotesList, int index, String newBallotTypeName){
		String templateContent = createTemplate(ballotTypes.get(index).getCandidateNames(), textFieldList, maxVotesList);
		String oldBallotTypeName = ballotTypes.get(index).getBallotTypeName(), newTemplatePath = "output/templates/" + newBallotTypeName + "_Template.txt";
		ballotTypes.get(index).setBallotTemplateContent(templateContent);
		ballotTypes.get(index).setBallotTypeName(newBallotTypeName);
		ballotTypes.get(index).setBallotTypeTemplatePath(newTemplatePath);
		ballotTypes.get(index).setIsSaved(true);
    	try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(ballotTypes.get(index).getBallotTypeTemplatePath())));
			writer.write(templateContent);
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	resetBallotData(oldBallotTypeName, newBallotTypeName, newTemplatePath);
	}
	
	public static void resetBallotData(String oldBallotTypeName, String newBallotTypeName, String newTemplatePath){
		for(int i = 0; i < BallotData.getInstance().getBallotType().size(); i++){
			if(BallotData.getInstance().getBallotType().get(i).equals(oldBallotTypeName)){
				BallotData.getInstance().getBallotType().set(i, newBallotTypeName);
				BallotData.getInstance().getCandidateFileArrayList().set(i, new CandidateFile(newTemplatePath));
			}
		}
	}
	
	public static String createTemplate(ArrayList<CandidateName> candidateNames, ArrayList<JTextField> textFieldList, ArrayList<JTextField> maxVotesList){
		String finalResult = "", newCandidateName = "", newMaxVote = ""; int positionCount = 0;
		for(int i = 0; i < candidateNames.size(); i++){
			newCandidateName = textFieldList.get(i).getText();
			if(candidateNames.get(i).getNewSet()){
				positionCount++;
				newMaxVote = positionCount == 5 ? maxVotesList.get(0).getText() : positionCount == 8 ? maxVotesList.get(1).getText() : candidateNames.get(i).getMaxVote();
				finalResult = finalResult.concat((i==0? "v-" : "\r\nv-") + newMaxVote + " " + candidateNames.get(i).getCandidatePosition() + "\r\n");
				finalResult = finalResult.concat(newCandidateName);
			}
			else if(candidateNames.get(i).getNewRow()){
				finalResult = finalResult.concat("\r\n" + newCandidateName);
			}
			else{
				finalResult = finalResult.concat(" // " + newCandidateName);
			}
			candidateNames.get(i).setMaxVote(newMaxVote);
			candidateNames.get(i).setCandidateName(newCandidateName);
			candidateNames.get(i).setIsNameCorrect(checkNameCorrect(newCandidateName) ? true : false);
		}
		return finalResult;
	}
	
	public static boolean checkNameCorrect(String candidateName){
		return correctNames.contains(candidateName) ? true : false;
	}
	
	public static String readResult(String fileName){
		String string = null;
		System.out.println("THE FILE IS: " + fileName);
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		  	string = sb.toString();
		    br.close();
		} catch(Exception e){
			JOptionPane.showMessageDialog(MainFrame.getInstance(),
				    "Invalid File Please Check selection",
				    "Invalid File",
				    JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return null;
		}
		return string;
	}
	
	public static Mat showInPanel(Mat srcImg){
		Imgproc.resize(srcImg.clone(), srcImg, new Size(400, 650));
		return srcImg;
	}
}
