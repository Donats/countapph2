/**
 * 
 */
package util;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JDialog;
import javax.swing.UIManager;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public abstract class Dialog extends JDialog implements MouseListener{
	
	public Dialog(Frame parent, boolean clickThroughable, int width, int height, boolean decorated, boolean resizable, Color color, FlowLayout layout){
		super(parent, !clickThroughable);
		UIManager.LookAndFeelInfo looks[] = UIManager.getInstalledLookAndFeels();
		try{
			UIManager.setLookAndFeel(looks[3].getClassName());
		} catch(Exception e){}
		setLayout(layout);
		setSize(width, height);
		setResizable(resizable);
		setUndecorated(!decorated);
		setBackground(color);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setVisible(false);
		addMouseListener(this);
	}
	
	public void init(){
        initComponents();
        addComponents();
	}
	
	public abstract void initComponents();
	public abstract void addComponents();
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("Mouse at: "+e.getX()+","+e.getY());
	}
}
