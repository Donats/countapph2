/**
 * 
 */
package util;

import java.awt.Color;
import java.awt.LayoutManager;

import javax.swing.JWindow;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class Window extends JWindow {
	
	  public Window(int width, int height, LayoutManager layout, boolean isVisible, Color bg){
	        super();
	        setSize(width, height);
	        setLayout(layout);
	        setLocationRelativeTo(null);
	        setVisible(isVisible);
	        if(bg != null)
	        	setBackground(bg);
	    }
	    
}
