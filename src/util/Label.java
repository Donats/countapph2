/**
 * 
 */
package util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class Label extends JLabel{
	public Label(ImageIcon icon, Point location, Dimension dimension){
    	super();
		if(icon != null){
			setIcon(icon);
			setSize(icon.getIconWidth(), icon.getIconHeight());
		}
		else
			setSize(dimension);
		setLocation(location);
    }
    
	public Label(ImageIcon icon, Point location){
		super();
		if(icon != null){
			setIcon(icon);
			setSize(icon.getIconWidth(), icon.getIconHeight());
		}
		setLocation(location);
	}

	public Label(String txt, Rectangle bounds, boolean preferred, int position) {
		super(txt);
		setBounds(bounds);
        if(preferred)
            setPreferredSize(bounds.getSize());
        //setVerticalAlignment(position);
        setHorizontalAlignment(position);
    }

	public Label() {
	}

	public void hideLabel() {
		setVisible(false);
	}
	
	public void showLabel(){
		setVisible(true);
	}
}
