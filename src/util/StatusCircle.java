/**
 * 
 */
package util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class StatusCircle extends JPanel{
	
	private Color color;

    public StatusCircle(Color color, Rectangle rect) {
        super();
        this.color = color;
        setBounds(rect);
        this.setBackground(null);
        this.setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //progress
        g.setColor(color);
        g.drawOval(10, 10, 10, 10);
        g.fillOval(10, 10, 10, 10);

    }

    public void setColor(Color color){
        this.color = color;
        repaint();
    }
}
