/**
 * 
 */
package util;

import java.awt.LayoutManager;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class Panel extends JPanel{

	public Panel(Rectangle bounds, LayoutManager layout){
		if(bounds!=null)
			setBounds(bounds);
		setLayout(layout);
	}
	
	public Panel(){}
}
