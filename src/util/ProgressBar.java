/**
 * 
 */
package util;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class ProgressBar extends JPanel {
    private Color color;
    private double minimum = 0;
    private double maximum = 100;
    private int value = 100;

    public ProgressBar(Color color, Rectangle rect) {
        super();
        this.color = color;
        setBounds(rect);
        setFont(Lib.getDefaultFont(13));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //border
        g.setColor(new Color(70, 70, 70));
        g.drawRect(0, 0, getWidth()-1, getHeight()-1);
        
        //background
        g.setColor(Color.WHITE);
        g.fillRect(1, 1, getWidth()-2, getHeight()-2);

        //progress
        int drawAmount = (int) (((value - minimum) / (maximum - minimum)) * getWidth());
        g.setColor(color);
        g.fillRect(1, 1, drawAmount-2, getHeight()-2); //-2 to account for border

        //string painting
        String stringToPaint = (int)value + "%";
        Canvas c = new Canvas();
        FontMetrics fm = c.getFontMetrics(Lib.getDefaultFont(15));
        final int stringWidth = fm.stringWidth(stringToPaint);
        final int stringHeight = fm.getHeight();
        g.setColor(new Color(222, 222, 222));
        g.drawString(stringToPaint, (getWidth()/2) - (stringWidth/2), ((getHeight()/2) + (stringHeight/2))-4); //-2 to account for border
    }

    public void setColor(Color color){
        this.color = color;
    }

    public void setMinimum(double minimum){
        this.minimum = minimum;
    }

    public void setMaximum(double maximum){
        this.maximum = maximum;
    }

    public void setValue(int value){
        this.value = value;
    }
    
}
