/**
 * 
 */
package util;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class Button extends JButton {
	
	public Button(ImageIcon icon, ImageIcon roIcon, Point location) {
		if(icon != null)
			setIcon(icon);
		if(roIcon != null){
			setRolloverIcon(roIcon);
			setSize(roIcon.getIconWidth(), roIcon.getIconHeight());
		}
		setLocation(location);
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setFocusable(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	public Button(String txt, Rectangle bounds) {
		super(txt);
		setBounds(bounds);
	}

	public Button(Rectangle bounds) {
		setBounds(bounds);
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setFocusable(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}
}
