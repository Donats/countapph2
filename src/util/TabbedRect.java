/**
 * 
 */
package util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class TabbedRect extends JPanel{
	
	private Color color;

    public TabbedRect(Color color, Rectangle rect) {
        super();
        this.color = color;
        setBounds(rect);
        this.setBackground(null);
        this.setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //progress
        g.setColor(color);
        g.drawRect(0, 0, getWidth(), getHeight());
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public void setColor(Color color){
        this.color = color;
        repaint();
    }

}
