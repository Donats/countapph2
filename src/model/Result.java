/**
 * 
 */
package model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import model.CandidateFile.Candidate;
import model.CandidateFile.TableOfCandidates;
import view.MainFrame;

/**
 * @author donatoamasa
 *
 */
public class Result {
	
	private boolean checkIfSummary;
	public Result(ArrayList<TableOfCandidates> tableOfCandidates, String resultFrom, boolean checkIfSummary){
		this.checkIfSummary = checkIfSummary;
		checkResult(tableOfCandidates, resultFrom);
	}
	
	public void checkResult(ArrayList<TableOfCandidates> tableOfCandidates, String resultFrom){
		String res = (checkIfSummary? "Summary result for " : "Result for ") + resultFrom;
		for(int i = 0; i < tableOfCandidates.size() ;i++){
			res = res + "\n\nRunning for: " +  tableOfCandidates.get(i).getPosition() + "\n\n";
			ArrayList<Candidate> candArrayList = tableOfCandidates.get(i).getCandidateArrayList();
			for(int j = 0 ; j < candArrayList.size() ;j++){
					res = res + candArrayList.get(j).getName()+ " has "+ candArrayList.get(j).getVote() + (candArrayList.get(j).getVote() > 1 ? " votes.\n" : " vote.\n");
			}
		}
		writeResult(res.replaceAll("\n", System.lineSeparator()), resultFrom);
	}
	
	public void writeResult(String result, String resultFrom){
		MainFrame.getInstance().resultsDialog.updateTextArea(result);
		try {
		    BufferedWriter fileOut = new BufferedWriter(new FileWriter("output/results/result_" + resultFrom + ".txt")); 		    
		    fileOut.write(result);
		    fileOut.close();
		} catch (IOException ioe) {
			System.out.println("error occured in IO");;
		    ioe.printStackTrace();
		}
	}
}
