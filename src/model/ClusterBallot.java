/**
 * 
 */
package model;

import static org.opencv.imgproc.Imgproc.getStructuringElement;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import view.ClusterPanel;
import view.MainFrame;
import view.MainFrame.LoadingDialog;
import view.MenuPanel;

/**
 * @author donatoamasa
 *
 */
public class ClusterBallot implements Runnable{

	/********************************************************/
	/***************CV THRESHOLD DEFAULT VALUES**************/
	/********************************************************/
	private static final int
	    CV_MOP_CLOSE = 3,
	    CV_THRESH_OTSU = 8,
	    CV_THRESH_BINARY = 0,
	    CV_ADAPTIVE_THRESH_GAUSSIAN_C  = 1,
	    CV_ADAPTIVE_THRESH_MEAN_C = 0,
	    CV_THRESH_BINARY_INV  = 1,
	    CV_THRESH_TRUNC = 2,
		CV_MORPH_RECT = 0;

	
	/********************************************************/
	/***************Image Preprocessing Types****************/
	/********************************************************/
	public static final String 
		APPLY_GRAYSCALE = "Grayscale_Ballot",
		APPLY_GAUSSIAN_BLUR = "Gaussian_Blurred_Ballot",
		APPLY_GAUSSIAN_BLUR_2 = "Gaussian_Blurred_Ballot_2",
		APPLY_OTSU_THRESHOLD = "Otsu_Threshold_Ballot",
		APPLY_TRUNC = "Truncate",
		APPLY_ADAPTIVE_MEAN_THRESHOLD = "Adaptive_Mean_Threshold_Ballot",
		APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD = "Adaptive_Gaussian_Threshold_Ballot",
		APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD_INV = "Adaptive_Gaussian_Threshold_Ballot_INV",
		APPLY_MORPHOLOGY = "Morphological_Ballot",
		APPLY_CONTOURS = "Contour_Ballot",
		APPLY_THRESHOLD_INV = "Threshold_Inv_Ballot",
		APPLY_DILATION_2 = "Dilate_Ballot_2",
		APPLY_EROSION = "Erode_Ballot",
		LOCATE_AREA = "Located_Area";
		
	/********************************************************/
	 /***************IMAGE PATHS AND NAMES****************/
	/********************************************************/
	
	public static final String 
		PREPROCESSED_IMAGE_PATH = "images/preprocessedImages/",
		FINAL_CROPPED_IMAGE_PATH = "images/croppedImages/",
		ORIGINAL_BALLOT = "Original_Ballot",
		CONVERTED_BALLOT = "Final_Cropped_Ballot" ;
	
	/********************************************************/
	/*******************CROPPING TYPPES**********************/
	/********************************************************/
	private static final int
		CONTOUR_CROP = 1,
		DETECTED_CROP = 0;
	
	private ArrayList<File> files;
	private ArrayList<DetectedImageData> finalCroppedImages;
	private ArrayList<FinalBallot> finalBallots;
	private ArrayList<BallotType> ballotType;
	private static ClusterBallot instance;
	public int order, positionCounter;
	
	public CandidateFile candidateFile;
	
	public boolean loop;
	private boolean saveImages;
	

	public Mat element;
	public Mat kernel, kernel2;
	private Rect rect;
	
	public ClusterBallot(){
		instance = this;
		initializeComponents();
	}
	
	public void initializeComponents(){
		order = 0;
		positionCounter = 0;
		loop = false;
		saveImages = false;
		files = new ArrayList<File>();
		finalBallots = new ArrayList<FinalBallot>();
		ballotType = new ArrayList<BallotType>();
		finalCroppedImages = new ArrayList<DetectedImageData>();
		element = getStructuringElement(CV_MORPH_RECT, new Size(12, 3));
		kernel = getStructuringElement(CV_MORPH_RECT, new Size(1, 1), new Point(0,0)); //size 1.75, 1.75 point 0.75 0.75
		kernel2 = getStructuringElement(CV_MORPH_RECT, new Size(3, 3), new Point(0,0)); //size 1.75, 1.75 point 0.75 0.75
		rect = new Rect();
	}
	
	public void updateFiles(ArrayList<File> files){
		if(!loop){
			if(files.isEmpty())
				this.files = files;
			else
				this.files.addAll(files);
		}
	}
	
	public ArrayList<BallotType> getBallotTypes(){
		return ballotType;
	}
	
	public void removeBallot(int index){
		files.remove(index);
	}
	
	public ArrayList<FinalBallot> getFinalBallots(){
		return finalBallots;
	}
	
	public ArrayList<File> getAllFiles(){
		return files;
	}
	
	public void clusterBallots(){
		finalBallots.clear();
		MenuPanel.getInstance().beforeClustering();
		for(int i = 0; i < files.size(); i++){
			Mat sourceImage = Imgcodecs.imread(files.get(i).getAbsolutePath());
			Rect [] croppedDimension = new Rect[2];
			sourceImage = getSkewAngle(sourceImage.clone());
			croppedDimension = getTable(sourceImage);			
			Mat croppedBallot = new Mat(sourceImage.clone(), croppedDimension[1]);
			Mat table = new Mat(sourceImage.clone(),croppedDimension[0]);
			if(isValidImage(table, i)){
				Mat tableContour = table.clone();
				Mat ballotPrecinctCluster = cropBallot2(croppedBallot);
				performPreprocessingForDensityRegion(tableContour);
				peformPreprocessingForBallotTypeOCR(ballotPrecinctCluster);
				String precinctCluster = PerformOCR.getInstance().checkOCR(DeskewImage.mat2Img(ballotPrecinctCluster));
				Ballot ballot = new Ballot(table);
				int ballotTypeIndex = getBallotType(precinctCluster, ballot);
				finalBallots.add(new FinalBallot(table, ballotTypeIndex, files.get(i).getName()));
			}
			LoadingDialog.getInstance().setProcessLabel("Processed Image: " + (i+1) + "/" + (files.size()));
			LoadingDialog.getInstance().setProgressBarValue((int)((double)(i+1)/files.size()*100));
		}
		animate(500);
		MenuPanel.getInstance().afterClustering();
		ClusterPanel.getInstance().updateBallotTypeNames(getBallotTypes());
		Model.getInstance().setFinalData(ballotType, finalBallots);
		loop = false;
	}
	
	
	private boolean isValidImage(Mat srcImg, int i){
		try{
			if(srcImg == null || srcImg.empty()){
				JOptionPane.showMessageDialog(MainFrame.getInstance(),
			    "An unexpected error occured on " + files.get(i).getName(),
			    "File Error",
			    JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}catch(Exception e){
			JOptionPane.showMessageDialog(MainFrame.getInstance(),
			"An unexpected error occured on " + files.get(i).getName(),
		    "File Error",
		    JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	

	public Mat cropBallot(Mat sourceImage, Rect cropped){
		Rect newRect = new Rect(cropped.x + (int)(cropped.width*0.54), cropped.y - (int)(cropped.y*0.39), (int)(cropped.width/3.9), (int)(cropped.y*(.13)));
		Mat croppedImage = new Mat(sourceImage, newRect);
		return croppedImage;
	}
	
	public Mat cropBallot2(Mat sourceImage){
		Rect newRect = new Rect((int)(sourceImage.width()*0.53), (int) (sourceImage.height()*0.125), (int)(sourceImage.width()/3.95), (int)(sourceImage.height()*0.03));
		Mat croppedImage = new Mat(sourceImage, newRect);
		return croppedImage;
	}
	
	public int getBallotType( String precinctCluster, Ballot ballot){
		String ballotFinalType = "Cluster_"+ballotType.size(), ballotTypeTemplatePath = "", ballotTemplateContent = ""; boolean exist = false;
		String namesAccuracy = ""; ArrayList<Rect> shadedOvals = new ArrayList<Rect>(); ArrayList<CandidateName> candidateNames = new ArrayList<CandidateName>();
		int ballotTypeIndex = ballotType.size();
		int edit_distance_threshold = 2;
		if(ballotType.isEmpty()){
			namesAccuracy = preprocessBallot(ballot,shadedOvals, candidateNames);
			ballotType.add(new BallotType(ballotTypeIndex, precinctCluster, ballotFinalType, ballotTypeTemplatePath, ballotTemplateContent, namesAccuracy, shadedOvals, candidateNames, "", false, true));
		}
		else{
			for(int i = 0; i < ballotType.size(); i++){
				int editDistanceResult = EditDistance.getFinalEditDistanceValue(precinctCluster, ballotType.get(i).getPrecinctCluster());
				System.out.println("ED Result: " + editDistanceResult);
				if(editDistanceResult <= edit_distance_threshold){
					ballotTypeIndex = ballotType.get(i).getBallotType();
					ballotType.get(i).setHasContent(true);
					ballotType.get(i).addPrecinctCluster(precinctCluster);
					exist = true;
					LoadingDialog.getInstance().setProgressBar2Value(100);
					LoadingDialog.getInstance().setProcessLabel2("Assigning ballot type to the ballot.");
					break;
				}
			}
			if(!exist){
				namesAccuracy = preprocessBallot(ballot, shadedOvals, candidateNames);
				ballotType.add(new BallotType(ballotTypeIndex, precinctCluster, ballotFinalType, ballotTypeTemplatePath, ballotTemplateContent, namesAccuracy, shadedOvals, candidateNames, "", false, true));
			}
		}
		return ballotTypeIndex;
	}
	
	public void performPreprocessingForDensityRegion(Mat ballotSource){
		preprocessImage(APPLY_GRAYSCALE, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_GAUSSIAN_BLUR, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_THRESHOLD_INV, ballotSource.clone(), ballotSource);
	}

	
	public void peformPreprocessingForBallotTypeOCR(Mat ballotSource){
		preprocessImage(APPLY_GRAYSCALE, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_GAUSSIAN_BLUR, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_TRUNC, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD_INV, ballotSource.clone(), ballotSource);
	}
	
	public void resetComponents(){
		finalCroppedImages.clear();
		positionCounter = 0;
	}
	
	public String preprocessBallot(Ballot ballot, ArrayList<Rect> shadedOvals, ArrayList<CandidateName> candidateNames){
		resetComponents();
		performPreprocessingForRegionDetection(ballot);
		detectCandidateCells(ballot, shadedOvals);
		loop = false;
		return PerformOCR.getInstance().doTheOCR(finalCroppedImages, positionCounter, candidateNames);
	}
	
	public void performPreprocessingForRegionDetection(Ballot ballot){
		saveImages = true;
		preprocessImage(APPLY_GRAYSCALE, ballot.getOriginal(), ballot.getGrayScaled());
		preprocessImage(APPLY_EROSION, ballot.getGrayScaled().clone(), ballot.getGrayScaled());
		preprocessImage(APPLY_THRESHOLD_INV, ballot.getGrayScaled(), ballot.getAdaptiveGaussianThreshold());
		ballot.setCountour(ballot.getAdaptiveGaussianThreshold().clone());
		Imgproc.findContours(ballot.getContour(), ballot.contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE); 
		Imgproc.drawContours(ballot.getContour(), ballot.contours, -1, new Scalar(0,0,255));
	}
	
	public void performPreprocessingForOCR(Mat ballotSource, int ballotCounter){
		saveImages = false;
		preprocessImage(APPLY_GRAYSCALE, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_GAUSSIAN_BLUR, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_TRUNC, ballotSource.clone(), ballotSource);
		preprocessImage(APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD_INV, ballotSource.clone(), ballotSource);
		Imgcodecs.imwrite(FINAL_CROPPED_IMAGE_PATH + (ballotCounter) + "_" +  CONVERTED_BALLOT + ".tif", ballotSource);
	}
	
	public void preprocessImage(String processType, Mat ballotSource, Mat ballotProcessed){
		if(processType.equals(APPLY_GRAYSCALE))
			Imgproc.cvtColor(ballotSource, ballotProcessed, Imgproc.COLOR_BGR2GRAY);
		else if(processType.equals(APPLY_GAUSSIAN_BLUR))
			Imgproc.GaussianBlur(ballotSource, ballotProcessed, new Size(3, 5),0); // 3, 5
		else if(processType.equals(APPLY_GAUSSIAN_BLUR_2))
			Imgproc.GaussianBlur(ballotSource, ballotProcessed, new Size(5, 5),0); // 3, 5
		else if(processType.equals(APPLY_OTSU_THRESHOLD))
			Imgproc.threshold(ballotSource, ballotProcessed, 0, 255,  CV_THRESH_OTSU + CV_THRESH_BINARY);
		else if(processType.equals(APPLY_TRUNC))
			Imgproc.threshold(ballotSource, ballotProcessed, 200, 255, CV_THRESH_TRUNC);
		else if(processType.equals(APPLY_ADAPTIVE_MEAN_THRESHOLD))
			Imgproc.adaptiveThreshold(ballotSource, ballotProcessed, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 11, 2);
		else if(processType.equals(APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD))
			Imgproc.adaptiveThreshold(ballotSource, ballotProcessed, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 11, 2);
		else if(processType.equals(APPLY_ADAPTIVE_GAUSSIAN_THRESHOLD_INV))
			Imgproc.adaptiveThreshold(ballotSource, ballotProcessed, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 11, 2); // 11, 2
		else if(processType.equals(APPLY_MORPHOLOGY))
			Imgproc.morphologyEx(ballotSource, ballotProcessed, CV_MOP_CLOSE, element); //или imgThreshold
		else if(processType.equals(APPLY_THRESHOLD_INV))
			Imgproc.threshold(ballotSource, ballotProcessed, 200, 255, Imgproc.THRESH_BINARY_INV);
		else if(processType.equals(APPLY_DILATION_2))
			Imgproc.dilate(ballotSource, ballotProcessed, kernel2);
		else if(processType.equals(APPLY_EROSION))
			Imgproc.erode(ballotSource, ballotProcessed, kernel2);
		if(saveImages)
			createProcessedBallotImage(processType, ballotProcessed);
	}
	
	public void detectCandidateCells(Ballot ballot, ArrayList<Rect> shadedOvals){
		int ballotCounter = 0; Rect prevRect = null; 
		ballot.setDetectedArea(ballot.getOriginal().clone());
		if (ballot.contours.size() > 0) {
            for (MatOfPoint matOfPoint : ballot.contours) {
                MatOfPoint2f points = new MatOfPoint2f(matOfPoint.toArray());
                RotatedRect box = Imgproc.minAreaRect(points);
                if(checkRatio(box)){
                	rect = box.boundingRect();
                	Imgproc.rectangle(ballot.getDetectedArea(), box.boundingRect().tl(), box.boundingRect().br(), new Scalar(0, 0, 255), 5);
                    ballot.detectedCropped = new Mat(ballot.getOriginal(), box.boundingRect());
                    ballot.detectedCroppedContour = new Mat(ballot.getAdaptiveGaussianThreshold(), box.boundingRect());
                    ballot.setDetectedCroppedContour(cropBallotImage(ballot.getDetectedCroppedContour(), rect, CONTOUR_CROP));
                    if(checkDensity(ballot.getDetectedCroppedContour())){
                    	addShadedOvals(rect, shadedOvals);
                    	ballot.setDetectedCropped(cropBallotImage(ballot.getDetectedCrop().clone(), rect, DETECTED_CROP));
                    	performPreprocessingForOCR(ballot.getDetectedCrop(), ballotCounter);
                    	addToFinalDetectedImages(FINAL_CROPPED_IMAGE_PATH, (ballotCounter) + "_" + CONVERTED_BALLOT + ".tif", prevRect, rect, ballotCounter, DeskewImage.mat2Img(ballot.getDetectedCrop()));
                    	ballotCounter++; prevRect = rect;
                    	Imgproc.rectangle(ballot.getDetectedArea(), box.boundingRect().tl(), box.boundingRect().br(), new Scalar(0, 255, 0), 5);
                    }
                }
            } 
        }
		createProcessedBallotImage(LOCATE_AREA, ballot.getDetectedArea());
	}
	
	private void addShadedOvals(Rect rect, ArrayList<Rect> shadedOvals){
		shadedOvals.add(new Rect(rect.x +(int) (rect.width*(0.025)) , rect.y+(int)(rect.height*(0.05)), (int) (rect.width*(0.15)), rect.height-(int)(rect.height*(0.15)) ));
	}
	
	public void addToFinalDetectedImages(String imagePath, String imageName, Rect prevRect, Rect newRect,  int ballotCounter, BufferedImage croppedImage){
		boolean newSet = false, newRow = false;
		if(prevRect != null && prevRect.y - newRect.y >= 30){
			if(prevRect.y - newRect.y >= 80){
				finalCroppedImages.get(ballotCounter-1).setNewSet(true);
				positionCounter++;
			}
			finalCroppedImages.get(ballotCounter-1).setNewRow(true);
		}
		LoadingDialog.getInstance().setProcessLabel2("New ballot type detected. Cropping image: " + imageName);
		//System.out.println("ImagePath: " + imagePath + "PrevY: " + (prevRect == null ? 0 : prevRect.y) + " Newy: " + newRect.y + " NewRow: "  + newRow + " NewSet: " + newSet + " ballotCounter: " + ballotCounter +"\n");
		finalCroppedImages.add(new DetectedImageData(imagePath, imageName, newRow, newSet, croppedImage));
		//animate(100);
	}
	
	public ArrayList<DetectedImageData> getFinalCroppedImagesData(){
		return finalCroppedImages;
	}
	
	public static boolean checkRatio(RotatedRect candidateImage) {
        // default error = 0.15 aspect = 6 min 25 max 125 aspect = 20 for colored label
		double error = 0.15;
        double aspect = 6;
        int min = 50 * (int)aspect * 50;
        int max = 125 * (int)aspect * 125;
        //Get only rects that match to a respect ratio.
        double rmin= aspect - aspect*error;
        double rmax= aspect + aspect*error;
        double area= candidateImage.size.height * candidateImage.size.width;
        float r= (float)candidateImage.size.width / (float)candidateImage.size.height;
        if(r<1)
            r= 1/r;
        if(( area < min || area > max ) || ( r < rmin || r > rmax )){
            return false;
        }else{
            return true;
        }
    }
	// 2013 = 10?
	public static boolean checkDensity(Mat candidateImage) {
        float whitePx = 0;
        float allPx = 0;
        double edge_density_threshold = 0.1;
        whitePx = Core.countNonZero(candidateImage);
        allPx = candidateImage.cols() * candidateImage.rows();
        // 0.62 previous value
        if ( whitePx/allPx >= edge_density_threshold)
            return true;
        else
            return false;
    }
	
	// DC sX = 0.14, sY = 0.10, s = 0.02, CC 0.05
	private static Mat cropBallotImage(Mat src, Rect rect, int cropType){
		int startX = 0, startY = 0, surplus = 0;
		if(cropType == DETECTED_CROP){
			startX = (int) (rect.width * (0.20));
			startY = (int) (rect.height * (0.09));
			surplus = (int) (rect.width * (0.04));
			//System.out.println("Rect.x = " + rect.x + " Rect.y = " + rect.y + " Rect.height = " + rect.height + " Rect.width = " + rect.width + " StartX = " + startX + " StartY = "  + startY);
		}
		else if(cropType == CONTOUR_CROP){
			startX = (int) (rect.width * (0.08));
			startY = (int) (rect.height *(0.08));
			surplus = (int) (rect.width * (0.08));
		}
		Rect newRect = new Rect(startX, startY, rect.width-(startX + surplus), rect.height- (int) (startY*1.7) );
		Mat cropped = new Mat(src, newRect);
		return cropped;
	}

	
	public void createProcessedBallotImage(String ballotName, Mat ballotSource1){
		Imgcodecs.imwrite(PREPROCESSED_IMAGE_PATH + (order++) + "_" +  ballotName + ".tif", ballotSource1);
		LoadingDialog.getInstance().setProcessLabel2("Preprocessing Image: " + ballotName);
	}
	
	public Mat showInPanel(Mat srcImg){
		Imgproc.resize(srcImg.clone(), srcImg, new Size(350, 420));
		return srcImg;
	}
	
	private Mat getSkewAngle(Mat sourceImage){
		double angle = 0;
		Mat source = sourceImage.clone();
		for(int i = 0; i < 2; i++){
			if(i == 1)
				Imgproc.Canny(source.clone(), source, 100, 150);
			double a = DeskewImage.doIt(DeskewImage.mat2Img(resize(source)));
			if((Math.abs(a) > Math.abs(angle))){
				angle = a;
			}
		}
		return deskew(sourceImage, angle);
	}
	
	private Mat deskew(Mat src, double angle){
		  Point center = new Point(src.width()/2, src.height()/2);
		    Mat rotImage = Imgproc.getRotationMatrix2D(center, angle, 1.0);
		    //1.0 means 100 % scale
		    Size size = new Size(src.width(), src.height());
		    Imgproc.warpAffine(src, src, rotImage, size );//, Imgproc.INTER_LINEAR + Imgproc.CV_WARP_FILL_OUTLIERS);
		    return src;
	}
	
	private Mat resize(Mat srcImg){
		if(srcImg.cols() <= 1500 || srcImg.rows() <= 1500){
			for(double i = 2 ; (i-0.5) * srcImg.cols() < 1500 || (i-0.5) * srcImg.rows() < 1500 ; i=i+0.5){
				if(i * srcImg.cols() > 1200 && i * srcImg.rows() > 1200)
					Imgproc.resize(srcImg.clone(), srcImg, new Size(srcImg.cols()*i, srcImg.rows()*i));
			}
		}
		return srcImg;
	}
	
	private Rect[] getTable(Mat srcImg){
		MatOfPoint pts = new MatOfPoint();
		Rect [] finalRects = new Rect [2];
		Rect cropped = new Rect();
		Mat bi = new Mat();
		Mat m = new Mat();
		Mat matrix = new Mat();
		Rect rectFirst = new Rect();
		ArrayList<MatOfPoint> cntr = new ArrayList<MatOfPoint>();
		Imgproc.cvtColor(srcImg.clone(), matrix, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(matrix.clone(), matrix, 200, 255, Imgproc.THRESH_BINARY_INV);
		for(int j = 1 ; j < 2 ; j++){
			m = matrix.clone();
			Imgproc.GaussianBlur(m.clone(), m, new org.opencv.core.Size (j*2 + 1, j*2 + 1), 1, 1);
			Imgproc.Canny(m.clone(), bi, 0, 50);
			ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();    
			Imgproc.findContours(bi.clone(),  contours, new Mat() ,Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			for(int i=0; i< contours.size();i++){
				if (Imgproc.contourArea(contours.get(i)) > (srcImg.cols()/2)  ){
		            Rect rect = Imgproc.boundingRect(contours.get(i));
		            if(cropped.area() < rect.area() && cropped.width < rect.width){
		            	cropped = rect;
		            	pts = contours.get(i);
		            	cntr.add(pts);
		            }
		        }
				if(Imgproc.contourArea(contours.get(i)) > 1000)
					rectFirst = Imgproc.boundingRect(contours.get(i));
		    }
		}
		int width =  rectFirst.width*28, height = rectFirst.height*40;
		if(width+rectFirst.x>srcImg.width())
			width = srcImg.width()-rectFirst.x;
		else if(height+rectFirst.y>srcImg.height())
			height = srcImg.height()-rectFirst.y;
		Rect ballotCrop = new Rect(rectFirst.x, rectFirst.y, width, height); 
		finalRects[0] = cropped;
		finalRects[1] = ballotCrop;
		return finalRects;
	}
	
	private void animate(long sleepTime){
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
	}
	
	public static ClusterBallot getInstance(){
		if(instance == null)
			instance = new ClusterBallot();
		return instance;
	}

	@Override
	public void run() {
		loop = true;
		while(loop){
			clusterBallots();
		}
	}
}
