/**
 * 
 */
package model;

import java.awt.image.BufferedImage;

/**
 * @author donatoamasa
 *
 */
public class DetectedImageData {

	private String imagePath;
	private String imageName;
	private boolean newRow;
	private boolean newSet;
	private BufferedImage image;
	public DetectedImageData(String imagePath, String imageName, boolean newRow, boolean newSet, BufferedImage image){
		this.imagePath = imagePath;
		this.imageName = imageName;
		this.newRow = newRow;
		this.newSet = newSet;
		this.image = image;
	}
	
	public void setImagePath(String imagePath){
		this.imagePath = imagePath;
	}
	
	public void setImageName(String imageName){
		this.imageName = imageName;
	}
	
	public void setNewRow(boolean newRow){
		this.newRow = newRow;
	}
	
	public void setNewSet(boolean newSet){
		this.newSet = newSet;
	}
	
	public void setImage(BufferedImage image){
		this.image = image;
	}
	
	public String getImagePath(){
		return imagePath;
	}
	
	public String getImageName(){
		return imageName;
	}
	
	public boolean getNewRow(){
		return newRow;
	}
	
	public boolean getNewSet(){
		return newSet;
	}
	
	public BufferedImage getImage(){
		return image;
	}
}
