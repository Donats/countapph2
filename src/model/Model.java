/**
 * 
 */
package model;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.SwingUtilities;

import view.MenuPanel;
import view.MainFrame.NotificationDialog;

/**
 * @author donatoamasa
 *
 */
public class Model {
	
	/********************************************************/
		/*************PROGRAM STATE VALUES**************/
	/********************************************************/
	public static final int 
		MAIN_STATE = -1,
		LOAD_STATE = 0,
		CLUSTER_STATE = 1,
		CHECK_STATE = 2,
		SAVE_STATE = 3;
	
	private static Model instance;
	private ClusterBallot clusterBallot;
	private ProcessBallot processBallot;
	public File[] ballotFiles;
	public int programState;
	private ArrayList<BallotType> finalBallotTypes;
	private ArrayList<FinalBallot> finalBallots;
	
	public Model(){
		instance = this;
		initialize();
	}
	
	public void initialize(){
		clusterBallot = new ClusterBallot();
		processBallot = new ProcessBallot();
		finalBallotTypes = new ArrayList<BallotType>();
		finalBallots = new ArrayList<FinalBallot>();
		programState = MAIN_STATE;
	}
	
	
	public void setFiles(File[] ballotFiles){
		if(ballotFiles != null){
			programState = LOAD_STATE;
			this.ballotFiles = ballotFiles;
			clusterBallot.updateFiles(new ArrayList<File>(Arrays.asList(ballotFiles)));
			SwingUtilities.invokeLater(new Runnable() {
			     public void run() {
					NotificationDialog.getInstance().showNotification(NotificationDialog.BALLOT_LOADED);
			     }
			});
			MenuPanel.getInstance().showLoadBallotsPanel(true);
		}
		else
			programState = MAIN_STATE;
	}
	
	public void doClusterBallots(){
		programState = CLUSTER_STATE;
		if(clusterBallot.loop)
			clusterBallot.loop = true;
		else
			(new Thread(clusterBallot)).start();
	}
	
	public void doCheckBallot(){
		programState = CHECK_STATE;
		if(processBallot.loop)
			processBallot.loop = true;
		else
			(new Thread(processBallot)).start();
	}
	
	public void setFinalData(ArrayList<BallotType> finalBallotTypes, ArrayList<FinalBallot> finalBallots){
		this.finalBallotTypes = finalBallotTypes;
		this.finalBallots = finalBallots;
	}
	
	public ArrayList<BallotType> getBallotTypes(){
		return finalBallotTypes;
	}
	
	public ArrayList<FinalBallot> getFinalBallots(){
		return finalBallots;
	}
	
	public boolean checkTemplates(){
		boolean allSaved = true;
		for(int i = 0; i < getBallotTypes().size(); i++){
			if(!getBallotTypes().get(i).getIsSaved())
				allSaved = false;
		}
		return allSaved;
	}
	
	public void doSummarize(){
		processBallot.summarize();
	}
	
	public void doShowBallotInfo(String imageName){
		processBallot.imageInfo(imageName);
	}
	
	public void doSave(){
		programState = SAVE_STATE;
	}
	
	public ArrayList<File> getFiles(){
		return clusterBallot.getAllFiles();
	}
	
	public void clearFiles(){
		clusterBallot.getAllFiles().clear();
	}
	
	public void clearBallotTypeContent(){
		if(!getBallotTypes().isEmpty())
			for(int i = 0; i < getBallotTypes().size(); getBallotTypes().get(i).setHasContent(false), i++);
	}
	
	public void deleteBallot(int index){
		clusterBallot.removeBallot(index);
	}
	
	public static Model getInstance(){
		if(instance == null)
			instance = new Model();
		return instance;
	}
}
