/**
 * 	An object that holds the data for the types of a ballot.
 */
package model;

import java.util.ArrayList;

import org.opencv.core.Rect;

/**
 * @author donatoamasa
 *
 */
public class BallotType {

	private int ballotType;
	private ArrayList<String> precinctClusters = new ArrayList<String>();
	private String ballotTypeName;
	private String ballotTypeTemplatePath;
	private String ballotTemplateContent;
	private String namesAccuracy;
	private ArrayList<Rect> shadedOvals;
	private ArrayList<CandidateName> candidateNames;
	private String summaryResultPath;
	private boolean isSaved;
	private boolean hasContent;
	
	public BallotType(int ballotType, String precinctCluster, String ballotTypeName, String ballotTypeTemplatePath, 
			String ballotTemplateContent, String namesAccuracy, ArrayList<Rect> shadedOvals, ArrayList<CandidateName> candidateNames, 
			String summaryResultPath, boolean isSaved, boolean hasContent){
		this.ballotType = ballotType;
		addPrecinctCluster(precinctCluster);
		this.ballotTypeName = ballotTypeName;
		this.ballotTypeTemplatePath = ballotTypeTemplatePath;
		this.ballotTemplateContent = ballotTemplateContent;
		this.namesAccuracy = namesAccuracy;
		this.shadedOvals = shadedOvals;
		this.candidateNames = candidateNames;
		this.summaryResultPath = summaryResultPath;
		this.isSaved = isSaved;
		this.hasContent = hasContent;
	}
	
	public void setBallotType(int ballotType){
		this.ballotType = ballotType;
	}
	
	public void addPrecinctCluster(String precinctCluster){
		if(precinctClusters.isEmpty())
			precinctClusters.add(precinctCluster);
		else
			for(int i = 0; i < precinctClusters.size(); i++)
				if(precinctClusters.get(i).equals(precinctCluster)){
					return;
				}
		precinctClusters.add(precinctCluster);
					
	}
	
	public void setBallotTypeName(String ballotTypeName){
		this.ballotTypeName = ballotTypeName;
	}
	
	public void setBallotTypeTemplatePath(String ballotTypeTemplatePath){
		this.ballotTypeTemplatePath = ballotTypeTemplatePath;
	}
	
	public void setBallotTemplateContent(String ballotTemplateContent){
		this.ballotTemplateContent = ballotTemplateContent;
	}
	
	public void setNamesAccuracy(String namesAccuracy){
		this.namesAccuracy = namesAccuracy;
	}
	
	public void setShadedOvals(ArrayList<Rect> shadedOvals){
		this.shadedOvals = shadedOvals;
	}
	
	public void setCandidateNames(ArrayList<CandidateName> candidateNames){
		this.candidateNames = candidateNames;
	}
	
	public void setSummaryResultPath(String summaryResultPath){
		this.summaryResultPath = summaryResultPath;
	}
	
	public void setIsSaved(boolean isSaved){
		this.isSaved = isSaved;
	}
	
	public void setHasContent(boolean hasContent){
		this.hasContent = hasContent;
	}
	
	public int getBallotType(){
		return ballotType;
	}
	
	public ArrayList<String> getPrecinctCluster(){
		return precinctClusters;
	}
	
	public String getBallotTypeName(){
		return ballotTypeName;
	}
	
	public String getBallotTypeTemplatePath(){
		return ballotTypeTemplatePath;
	}
	
	public String getBallotTemplateContent(){
		return ballotTemplateContent;
	}
	
	public ArrayList<Rect> getShadedOvals(){
		return shadedOvals;
	}
	
	public ArrayList<CandidateName> getCandidateNames(){
		return candidateNames;
	}
	
	public String getSummaryResultPath(){
		return summaryResultPath;
	}
	
	public String getNamesAccuracy(){
		return namesAccuracy;
	}
	
	public boolean getIsSaved(){
		return isSaved;
	}
	
	public boolean getHasContent(){
		return hasContent;
	}
}
