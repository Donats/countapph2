/**
 * 
 */
package model;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import model.CandidateFile.Candidate;
import model.CandidateFile.TableOfCandidates;
import view.MainFrame;
import view.MainFrame.LoadingDialog;
import view.MenuPanel;
import view.ResultsPanel;

/**
 * @author donatoamasa
 *
 */
public class ProcessBallot implements Runnable{
	
	private static ProcessBallot instance;
	public ArrayList<CandidatePoints> candPoints;
	public CandidateFile candidateFile;
	boolean loop = false;
	
	public ProcessBallot(){
		initializeComponents();
	}
	
	public void initializeComponents(){
		candPoints = new ArrayList<CandidatePoints>();
	}
	
	public void checkAndCountBallots(){
		ArrayList<FinalBallot> finalBallots = Model.getInstance().getFinalBallots();
		ArrayList<BallotType> ballotType = Model.getInstance().getBallotTypes();
		MenuPanel.getInstance().beforeCheckingAndCounting();
		BallotData.getInstance().clearArrays();
		for(int i = 0, counter = 0; i < ballotType.size();i++){
			candidateFile = new CandidateFile(ballotType.get(i).getBallotTypeTemplatePath());
			for(int j = 0; j < finalBallots.size(); j++){
				if(ballotType.get(i).getBallotType() == finalBallots.get(j).getBallotType()){
					initCandPoint(candidateFile);
					candidateFile.resetTableOfCandidatesVote();
					Mat tableMat = finalBallots.get(j).getOriginalImage();
					findContours(tableMat, ballotType.get(i).getShadedOvals());
					Mat finalMat = tableMat;
					markBallot(finalMat, j, ballotType.get(i).getBallotTypeName(), ballotType.get(i).getShadedOvals());
					BallotData.getInstance().getImgName().add(finalBallots.get(j).getFileName());
					BallotData.getInstance().getCandidateFileArrayList().add(candidateFile);
					BallotData.getInstance().getBallotType().add(ballotType.get(i).getBallotTypeName());
					LoadingDialog.getInstance().setProgressBar2Value((int)((double)(j+1)/finalBallots.size()*100));
					LoadingDialog.getInstance().setProcessLabel2("Checking " + ballotType.get(i).getBallotTypeName() + " ballots. "+ "Processing image " + finalBallots.get(j).getFileName());
					counter++;
				}
				LoadingDialog.getInstance().setProgressBarValue((int)((double)(counter)/finalBallots.size()*100));
				LoadingDialog.getInstance().setProcessLabel("Processing Image: " + (counter) + "/" + (finalBallots.size()));
				
			}
		}
		loop = false;
		animate(500);
		MenuPanel.getInstance().afterCheckingAndCounting();
		ResultsPanel.getInstance().updateBallotTypeComboBox(ballotType);
	}
	
	private void markBallot(Mat srcImg,int counter, String ballotType, ArrayList<Rect> shadedOvals){
		Scalar clr = new Scalar(0,255,0);
		correctNumOfVotes();
		for(int j = 0 ; j < candPoints.size() ;j++){
			if(candPoints.get(j).getCount() >= 5){
				Point p = candPoints.get(j).getPoint();
				if(checkColor(p.x, p.y, j))
					clr = new Scalar(0,255,0);
				else
					clr = new Scalar(0,0,255);
				//if(p.x <= 1)
				//	Imgproc.rectangle(srcImg, new Point(p.x*(srcImg.cols()/4)+ (srcImg.cols()/90),p.y * (srcImg.rows()/candidateFile.maxRows) ) , new Point((p.x*(srcImg.cols()/4))+((srcImg.cols()/4)*0.20), (p.y+1)* (srcImg.rows()/candidateFile.maxRows)), clr, 3);
				//else
				//	Imgproc.rectangle(srcImg, new Point(p.x*(srcImg.cols()/4)+ (srcImg.cols()/150) ,p.y * (srcImg.rows()/candidateFile.maxRows)  ) , new Point((p.x*(srcImg.cols()/4))+((srcImg.cols()/4)*0.20), (p.y+1)* (srcImg.rows()/candidateFile.maxRows)), clr, 3);
				Imgproc.rectangle(srcImg, shadedOvals.get(shadedOvals.size()-j-1).br() , shadedOvals.get(shadedOvals.size()-j-1).tl(), clr, 3);
			}
		}
		BallotData.getInstance().getcandPtsArray().add(candPoints);
		BallotData.getInstance().getEditImg().add(srcImg);
		Imgcodecs.imwrite("output/finalBallots/" + ballotType + "_" + counter + ".jpg" , srcImg);
	}
	
	private boolean checkColor(double x, double y, int k){
		ArrayList<TableOfCandidates> tableOfCandidates = candidateFile.tableOfCandidates;
		for(int i = 0; i < tableOfCandidates.size() ;i++){
			ArrayList<Candidate> candArrayList = tableOfCandidates.get(i).getCandidateArrayList();
			for(int j = 0 ; j < candArrayList.size() ; j++){
				int xx = candArrayList.get(j).getCol();
				int yy = candArrayList.get(j).getRow();
				if(candPoints.get(k).getPoint().equals(new Point(xx,yy))){
					if(tableOfCandidates.get(i).getVotes() <= tableOfCandidates.get(i).getMaxVotes()){
						
						if(candPoints.get(k).getCount() >= 5 && candPoints.get(k).getPoint().equals(new Point(x,y))){
							return true;
						}
					}
					else{
						return false;
					}
				}
			}
		}
		return false;
	}
	
	private void correctNumOfVotes(){
		for(int j = 0 ; j < candPoints.size() ;j++){
			if(candPoints.get(j).getCount() >= 5){
				Point p = candPoints.get(j).getPoint();
				checkNumOfVotes(p, candidateFile);
			}
		}
	}
	
	private void checkNumOfVotes(Point p, CandidateFile candidateFile){
		
		ArrayList<TableOfCandidates> tableOfCandidates = candidateFile.tableOfCandidates;
		for(int i = 0; i < tableOfCandidates.size() ;i++){
			if(p.y < tableOfCandidates.get(i).getRow()){
				ArrayList<Candidate> candArrayList = tableOfCandidates.get(i-1).getCandidateArrayList();
				for(int j = 0; j < candArrayList.size() ;j++){
					if(candArrayList.get(j).getRow() == p.y && candArrayList.get(j).getCol() == p.x){
						tableOfCandidates.get(i-1).addVoteCount();
						return;
					}
				}
			}
		}
		ArrayList<Candidate> candArrayList = tableOfCandidates.get(tableOfCandidates.size()-1).getCandidateArrayList();
		for(int j = 0; j < candArrayList.size() ;j++){
			if(candArrayList.get(j).getRow() == p.y && candArrayList.get(j).getCol() == p.x){
				tableOfCandidates.get(tableOfCandidates.size()-1).addVoteCount();
				return;
			}
		}
	}
	
	public void findContours(Mat srcMat, ArrayList<Rect> shadedOvals){
		Mat m = new Mat();				
		Imgproc.GaussianBlur(srcMat.clone(), m, new org.opencv.core.Size (3,3), 1, 1);
		//Imgcodecs.imwrite("images/Gaussian.jpg", m);
		for(int j = 0; j < 5 ; j++){
			Mat thres = new Mat();
			Imgproc.threshold(m.clone(), thres, 50+(j*10), 255, 1);
			checkBallotPos(thres.clone(), j, shadedOvals);
		}
		System.gc();
	}
	
	private void checkBallotPos(Mat srcImg, int j, ArrayList<Rect> shadedOvals){
		Mat result = new Mat();
		Imgproc.Canny(srcImg, result, 0, 50);
		//Imgcodecs.imwrite("images/canny" + j + ".png", result);
		for(int i = 0 ; i < candPoints.size() ; i++){
			//Point p = candPoints.get(i).getPoint();
			//Rect shadeArea = new Rect();
			Rect shadeArea = shadedOvals.get(shadedOvals.size()-i-1);
			//if(p.x != 0)
		//		shadeArea = new Rect(new Point(p.x*(srcImg.cols()/4)+ (srcImg.cols()/90),p.y* (srcImg.rows()/candidateFile.maxRows)  ) , new Point((p.x*(srcImg.cols()/4))+((srcImg.cols()/4)*0.20), (p.y+1)* (srcImg.rows()/candidateFile.maxRows) ));
			//else
			//	shadeArea = new Rect(new Point(p.x*(srcImg.cols()/4)+ (srcImg.cols()/90) +5,p.y * (srcImg.rows()/candidateFile.maxRows)   ) , new Point((p.x*(srcImg.cols()/4))+((srcImg.cols()/4)*0.20), (p.y+1)* (srcImg.rows()/candidateFile.maxRows) ));
			Mat cropped = cropImage(result, shadeArea);
			//Imgcodecs.imwrite("new" + i + ".tif", cropped);
			ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Imgproc.findContours(cropped.clone(),  contours, new Mat() ,Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
			for(int cc = 0 ; cc < contours.size() ; cc++){
				Rect rect = Imgproc.boundingRect(contours.get(cc));
				if( isCloseToCenter(rect, new Rect(new Point(cropped.cols()/4, cropped.rows()/4), new Point(cropped.cols()/4*3, cropped.rows()/4*3)))){
					candPoints.get(i).addCount();
				}
			}
		}
	}
	
	private static boolean isCloseToCenter(Rect rect, Rect center){
		if(rect.x < center.x + center.width && rect.x + rect.width > center.x && rect.y < center.y + center.height && rect.y + rect.height > center.y){
			return true;
		}
		return false;
	}
	
	private static Mat cropImage(Mat src, Rect rect){
		Mat cropped = new Mat(src, rect);
		return cropped;
	}
	
	public static Mat fitInReview(Mat srcImg){
		Imgproc.resize(srcImg.clone(), srcImg, new Size(600, 660));
		return srcImg;
	}
	
	private void initCandPoint(CandidateFile candidateFile){
		candPoints = new ArrayList<CandidatePoints>();
		for(int i = 0; i < candidateFile.tableOfCandidates.size(); i++){
			ArrayList<Candidate> candidateArrayList = candidateFile.tableOfCandidates.get(i).getCandidateArrayList();
			for(int j = 0 ; j < candidateArrayList.size() ; j++ ){
				Candidate cand = candidateArrayList.get(j);
				candPoints.add(new CandidatePoints(new Point(cand.getCol(), cand.getRow())));
			}
		}
	}
	
	public void summarize(){
		ArrayList<BallotType> ballotType = Model.getInstance().getBallotTypes();
		for(int counter = 0; counter < ballotType.size(); counter++){
			candidateFile = new CandidateFile(ballotType.get(counter).getBallotTypeTemplatePath());
			candidateFile.resetAllVotes();
			for(int i = 0 ; i < BallotData.getInstance().getEditImg().size() ; i++){
				if(ballotType.get(counter).getBallotTypeName().equals(BallotData.getInstance().getBallotType().get(i))){
					ArrayList<CandidatePoints> candPoints = BallotData.getInstance().getcandPtsArray().get(i);
					candidateFile.resetTableOfCandidatesVote();
					boolean exclude = false;
					for(int k = 0 ; k < BallotData.getInstance().getExcludeList().size() ; k++){
						if(BallotData.getInstance().getExcludeList().get(k) == BallotData.getInstance().getImgName().get(i))
							exclude = true;
					}
					if(!exclude){
						for(int j = 0 ; j < candPoints.size() ; j++){
							if(candPoints.get(j).getCount() >= 5){
								Point p = candPoints.get(j).getPoint();
								checkNumOfVotes(p, candidateFile);
							}
						}
						new TallyVotes(candPoints, candidateFile);
					}
				}
			}
			ballotType.get(counter).setSummaryResultPath("output/results/result_" + ballotType.get(counter).getBallotTypeName() + ".txt");
			new Result(candidateFile.tableOfCandidates, ballotType.get(counter).getBallotTypeName(), true);
		}
		//MainFrame.getInstance().promptDialog.showTabbedPaneList(0, ballotType);
		MainFrame.getInstance().resultsDialog.updateBallotTypeComboBox(ballotType);
		MainFrame.getInstance().resultsDialog.showResultsDialog(true);
	}
	
	
	public void imageInfo(String imageName){
		for(int i = 0; i < BallotData.getInstance().getImgName().size(); i++){
			if(BallotData.getInstance().getImgName().get(i).equals(imageName)){
				CandidateFile candidateFile = BallotData.getInstance().getCandidateFileArrayList().get(i);
				candidateFile.resetAllVotes();
				ArrayList<CandidatePoints> candPoints = BallotData.getInstance().getcandPtsArray().get(i);
				for(int j = 0 ; j < candPoints.size() ;j++){
					if(candPoints.get(j).getCount() >= 5){
						Point p = candPoints.get(j).getPoint();
						checkNumOfVotes(p, candidateFile);
					}
				}
				new TallyVotes(candPoints, candidateFile);
				new Result(candidateFile.tableOfCandidates, imageName, false);
			}
		}
		//MainFrame.getInstance().promptDialog.showDialog(0, true);
		MainFrame.getInstance().resultsDialog.showResultsDialog(false);
	}
	
	public class CandidatePoints{
		private Point point; //x = cand.cols , y = cand.rows
		private int count = 0;
		
		public CandidatePoints(Point p){
			point = p;
		}
		
		public Point getPoint() {
			return point;
		}
		public void setPoint(Point point) {
			this.point = point;
		}
		
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
		public void addCount(){
			this.count++;
		}
		
		public boolean compare(Point p){
			if(point.equals(p)){
				count++;
				return true;
			}
			return false;
		}
		
	}
	
	private void animate(long sleepTime){
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
	}
	
	public static ProcessBallot getInstance(){
		if(instance == null)
			instance = new ProcessBallot();
		return instance;
	}

	@Override
	public void run() {
		loop = true;
		while(loop){
			checkAndCountBallots();
		}
	}
}
