/**
 * 
 */
package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import view.MenuPanel;

/**
 * @author donatoamasa
 *
 */
public class CandidateFile {
	
	public ArrayList<TableOfCandidates> tableOfCandidates = new ArrayList<TableOfCandidates>();
	public int maxRows = 0;
	
	public CandidateFile(){
		
	}
	
	public CandidateFile(String fileName){
		String string = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
		
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		  	string = sb.toString();
		    br.close();
		    
		    
		} catch(Exception e){
			MenuPanel.getInstance().errorOccured();
			//JOptionPane.showMessageDialog(MainFrame.getInstance(),
			//	    "Invalid File Please Check selection",
			//	    "Invalid File",
			//	    JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
		
		String[] lines = string.split(System.getProperty("line.separator"));
		maxRows = lines.length;
		
		try{
			for(int i = 0 ; i<lines.length ;i++){
				lines[i] = lines[i].trim();
		    	if(!(lines[i].charAt(0) == 'v' && lines[i].charAt(1) == '-')){
		    		String str = lines[i].trim();
		    		int st = 0,  col = 0;
		    		for(int j = 0; j < str.length() ; j++){
		    			if((str.charAt(j) == '/' && str.charAt(j+1) == '/')){
		    				tableOfCandidates.get(tableOfCandidates.size()-1).getCandidateArrayList().add(new Candidate(str.substring(st, j).trim(), i, col));
		    				col++;
		    				st = j+2;
		    			}
		    		}
		    		tableOfCandidates.get(tableOfCandidates.size()-1).getCandidateArrayList().add(new Candidate(str.substring(st, str.length()).trim(), i, col));
		    	}
		    	else{
		    		for(int j = 2; j < lines[i].length() ;j++){
		    			if(lines[i].charAt(j) == ' '){
		    				tableOfCandidates.add(new TableOfCandidates(i , Integer.parseInt(lines[i].substring(2,j).trim()), lines[i].substring(j).trim()));
		    				break;
		    			}
		    		}
		    	}
		    }
		}catch(Exception e){
			MenuPanel.getInstance().errorOccured();
			//JOptionPane.showMessageDialog(MainFrame.getInstance(),
			//	    "Invalid File Contents",
			//	    "Invalid File",
			//	    JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
	}
	
	
	public void resetTableOfCandidatesVote(){
		for(int i = 0 ; i < tableOfCandidates.size() ; i++){
			tableOfCandidates.get(i).setVotes(0);
		}
	}
	
	public void resetAllVotes(){
		for(int i = 0 ; i < tableOfCandidates.size() ; i++){
			tableOfCandidates.get(i).setVotes(0);
			for(int j = 0 ; j < tableOfCandidates.get(i).getCandidateArrayList().size() ; j++){
				tableOfCandidates.get(i).getCandidateArrayList().get(j).setVote(0);
			}
		}
	}
	
	public class TableOfCandidates{
		
		private String position = "";
		private ArrayList<Candidate> candidate = new ArrayList<>();
		private int row, maxVotes , votes;
		
		public TableOfCandidates(int row, int maxVotes, String position){
			this.row = row;
			this.maxVotes = maxVotes;
			this.position = position;
		}

		public String getPosition(){
			return position;
		}
		
		public void setPosition(String pos){
			position = pos;
		}
		
		public int getMaxVotes() {
			return maxVotes;
		}

		public void setMaxVotes(int maxVotes) {
			this.maxVotes = maxVotes;
		}

		public int getVotes() {
			return votes;
		}

		public void setVotes(int votes) {
			this.votes = votes;
		}
		
		public void addVoteCount(){
			this.votes++;
		}

		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public ArrayList<Candidate> getCandidateArrayList() {
			return candidate;
		}

		public void setCandidateArrayList(ArrayList<Candidate> candidate) {
			this.candidate = candidate;
		}
		
	}
	
	public class Candidate{
		private int row = 0, col = 0, votes = 0;
		private String name;
		
		
		public Candidate(String name, int row, int col){
			this.setName(name);
			this.col = col;
			this.row = row;
		}

		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		public void addVote(){
			votes++;
		}
		
		public void setVote(int num){
			votes = num;
		}
		
		public int getVote(){
			return votes;
		}
	}
}

