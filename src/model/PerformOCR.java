/**
 * 
 */
package model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;


import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import util.Lib;
import view.MainFrame.LoadingDialog;

/**
 * @author donatoamasa
 *
 */
public class PerformOCR{

	private static PerformOCR instance;
	private ITesseract tesseractInstance, checkInstance;
	private String finalResult;
	private int correctNames = 0, charTotal = 0;
	
	public PerformOCR(){
		instance = this;
		initialize();
	}
	
	public void initialize(){
		finalResult = "";
		tesseractInstance = new Tesseract();
		checkInstance = new Tesseract();
		//tesseractInstance.setLanguage("eng");
		tesseractInstance.setTessVariable("tessedit_char_whitelist", "ABCDEFGHIJKMN�OPRSTUVWXYZQL1234567890(),.-");
		checkInstance.setTessVariable("tessedit_char_whitelist", "ABCDEFGHIJKMN�OPRSTUVWXYZQL1234567890,");
		
	}
	
	public String checkOCR(BufferedImage image){
		String result = "";
		try{
			result = checkInstance.doOCR(image).replace("\n", "").replaceAll("\r", "").replaceAll(" ", "").replace(",", "").trim();
		} catch (TesseractException e){
			System.err.println(e.getMessage());
		}	
		System.out.println(result);
		return result;
	}
	
	public String doTheOCR(ArrayList<DetectedImageData> finalCroppedImagesData, int positionCounter, ArrayList<CandidateName> candidateNames){
		correctNames = 0;
		candidateNames.clear();
		String namesAccuracy = "";
		String [] positions = (positionCounter == 4 ? Lib.POSITION_CITY_DISTRICT : Lib.POSITION_TOWNS);
		String result = ""; int newPositionCounter = 0;
		for(int i = finalCroppedImagesData.size(); i > 0; i--){
			//croppedImage = new File(finalCroppedImagesData.get(i-1).getImagePath()+finalCroppedImagesData.get(i-1).getImageName());
			try{
				newPositionCounter = finalCroppedImagesData.get(i-1).getNewSet() ? newPositionCounter+1 : newPositionCounter;
				result = tesseractInstance.doOCR(finalCroppedImagesData.get(i-1).getImage()).replace("\n", " ").replace("\r", "").trim();
				result = checkAccuracy(candidateNames, result, positions[newPositionCounter].substring(positions[newPositionCounter].indexOf(" ")+1).trim(), 
						positions[newPositionCounter].substring(0, positions[newPositionCounter].indexOf(" ")).trim(),
						i == finalCroppedImagesData.size() ? true : finalCroppedImagesData.get(i-1).getNewRow(), i == finalCroppedImagesData.size() ? true : finalCroppedImagesData.get(i-1).getNewSet());
				charTotal += result.trim().length();
			} catch (TesseractException e){
				System.err.println(e.getMessage());
			}
			LoadingDialog.getInstance().setProgressBar2Value((int)((double)(finalCroppedImagesData.size()-i+1)/finalCroppedImagesData.size()*100));
			LoadingDialog.getInstance().setProcessLabel2("Performing OCR on the image: " + finalCroppedImagesData.get(i-1).getImageName());
		}
		LoadingDialog.getInstance().setProcessLabel2("OCR done on new ballot type.");
		namesAccuracy = getAccuracy(candidateNames);
		System.out.println(charTotal);
		return namesAccuracy;
	}
	
	
	public String checkAccuracy(ArrayList<CandidateName> candidateNames, String resultName, String position, String maxVote, boolean newRow, boolean newSet){
		String newResultName = editName(resultName.replace(",", "").replace(".", "")).trim();
		if(Lib.correctNames.contains(newResultName)){
			correctNames++;
			candidateNames.add(new CandidateName(position, newResultName, maxVote, true, newRow, newSet));
		}
		else
			candidateNames.add(new CandidateName(position, newResultName, maxVote, false, newRow, newSet));
		return newResultName;
	}
	
	public String editName(String candidateName){
		candidateName = removeNumbers(candidateName);
		return fixPartyName(candidateName);
	}
	
	private String removeNumbers(String candidateName){
		String [] test = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		String nameSubString = "";
		nameSubString = candidateName.substring(2);
		candidateName = candidateName.substring(0, 2);
		for(int i = 0; i < test.length; i++){
			if(test[i].equals("1"))
				if(nameSubString.substring(nameSubString.indexOf("1")+1).equals("ND)"))
					nameSubString = nameSubString.replace("1", "I");
				else
					candidateName = candidateName.replace(test[i], "");
			else{
				candidateName = candidateName.replace(test[i], "");
			}
		}
		candidateName = candidateName.concat(nameSubString);
		// Remove excess letters before the name
		if(candidateName.indexOf(" ") == 1){
			candidateName = candidateName.substring(candidateName.indexOf(" ")+1);
		}
		return candidateName;
	}
	
	private String fixPartyName(String candidateName){
		String parenthesis = "";
		//Removes spaces inside the parenthesis
		if(candidateName.indexOf("(") != -1){
			parenthesis = candidateName.substring(candidateName.indexOf("(")).replace(" ", "");
			candidateName = candidateName.substring(0, candidateName.indexOf("(")).concat(parenthesis);
		}
		return candidateName.replace("(NFC)", "(NPC)").replace(" ND)", " (IND)").replace("(ND)", "(IND)").replace("0ND)", "(IND)").replace("  ", "");
	}
	
	public String getAccuracy(ArrayList<CandidateName> candidateNames){
		String finalNamesAccuracy, position, oldPosition = candidateNames.get(0).getCandidatePosition();
		finalNamesAccuracy = "Correct names: " + correctNames + "/" + candidateNames.size() + "	Accuracy: " + (float)(correctNames*100/candidateNames.size()) + "%" 
		+ "\n\nIncorrect Names: \n\n" + oldPosition;
		for(int i = 0; i < candidateNames.size(); i++){
			if(!candidateNames.get(i).getIsNameCorrect()){
				position = candidateNames.get(i).getCandidatePosition();
				finalNamesAccuracy = finalNamesAccuracy.concat( (position.equals(oldPosition) ? "" : "\n\n"+position) + "\n " + candidateNames.get(i).getCandidateName());
				oldPosition = position;
			}
		}
		oldPosition = candidateNames.get(0).getCandidatePosition();
		finalNamesAccuracy = finalNamesAccuracy.concat("\n\nCorrect Names: \n\n" + oldPosition);
		for(int i = 0; i < candidateNames.size(); i++){
			if(candidateNames.get(i).getIsNameCorrect()){
				position = candidateNames.get(i).getCandidatePosition();
				finalNamesAccuracy = finalNamesAccuracy.concat((position.equals(oldPosition) ? "" : "\n\n"+position) + "\n " + candidateNames.get(i).getCandidateName());
				oldPosition = position;
			}
		}
		return finalNamesAccuracy;
	}
	
	public String getFinalResult(){
		return finalResult;
	}
	
	public static PerformOCR getInstance(){
		if(instance == null)
			instance = new PerformOCR();
		return instance;
	}
}
