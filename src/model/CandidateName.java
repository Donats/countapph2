/**
 * 
 */
package model;

/**
 * @author donatoamasa
 *
 */
public class CandidateName {
	
	private String candidatePosition;
	private String candidateName;
	private String maxVote;
	private boolean isNameCorrect;
	private boolean newRow;
	private boolean newSet;
	public CandidateName(String candidatePosition, String candidateName, String maxVote, boolean isNameCorrect, boolean newRow, boolean newSet){
		this.candidatePosition = candidatePosition;
		this.candidateName = candidateName;
		this.maxVote = maxVote;
		this.isNameCorrect = isNameCorrect;
		this.newRow = newRow;
		this.newSet = newSet;
	}
	
	public void setCandidatePosition(String candidatePosition){
		this.candidatePosition = candidatePosition;
	}
	
	public void setCandidateName(String candidateName){
		this.candidateName = candidateName;
	}
	
	public void setMaxVote(String maxVote){
		this.maxVote = maxVote;
	}
	
	public void setIsNameCorrect(boolean isNameCorrect){
		this.isNameCorrect = isNameCorrect;
	}
	
	public void setNewRow(boolean newRow){
		this.newRow = newRow;
	}
	
	public void setNewSet(boolean newSet){
		this.newSet = newSet;
	}
	
	public String getCandidatePosition(){
		return candidatePosition;
	}
	
	public String getCandidateName(){
		return candidateName;
	}
	
	public String getMaxVote(){
		return maxVote;
	}
	
	public boolean getIsNameCorrect(){
		return isNameCorrect;
	}
	
	public boolean getNewRow(){
		return newRow;
	}
	
	public boolean getNewSet(){
		return newSet;
	}

}
