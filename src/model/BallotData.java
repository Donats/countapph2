/**
 *  An object used for the checking the ballot and getting the final result.
 */
package model;

import java.util.ArrayList;

import org.opencv.core.Mat;

import model.ProcessBallot.CandidatePoints;


/**
 * @author donatoamasa
 *
 */
public class BallotData {

	
	private static BallotData ballotData;
	
	ArrayList<String> imgName = new ArrayList<String>();
	ArrayList<Mat> editImg = new ArrayList<Mat>();
	ArrayList<ArrayList<CandidatePoints>> candidatePtsArray = new ArrayList<ArrayList<CandidatePoints>>();
	ArrayList<CandidateFile> candidateFileArrayList = new ArrayList<CandidateFile>();
	ArrayList<String> ballotType = new ArrayList<String>();
	ArrayList<String> excludeList = new ArrayList<String>();
	ArrayList<String> isChecked = new ArrayList<String>();
	
	public BallotData(){
	
	}
	
	public static BallotData getInstance(){
		if(ballotData==null)
			ballotData = new BallotData();
		return ballotData;
	}
	
	public ArrayList<CandidateFile> getCandidateFileArrayList(){
		return candidateFileArrayList;
	}
	
	public ArrayList<String> getBallotType(){
		return ballotType;
	}
	
	public void setImgName(ArrayList<String> imgNm){
		imgName = imgNm;
	}
	
	public void setEditImg(ArrayList<Mat> edtImg){
		editImg = edtImg;
	}
	
	public ArrayList<String> getImgName(){
		return imgName;
	}
	
	public ArrayList<Mat> getEditImg(){
		return editImg;
	}
	
	public ArrayList<ArrayList<CandidatePoints>> getcandPtsArray(){
		return candidatePtsArray;
	}
	
	public ArrayList<String> getExcludeList(){
		return excludeList;
	}
	
	public void clearArrays(){
		imgName.clear();
		editImg.clear();
		candidatePtsArray.clear();
		ballotType.clear();
		//candidateFile.resetAllVotes();
		candidateFileArrayList.clear();
	}
	
	
}
