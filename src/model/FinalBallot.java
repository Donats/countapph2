/**
 * 
 */
package model;

import org.opencv.core.Mat;

/**
 * @author donatoamasa
 *
 */
public class FinalBallot {

	private Mat originalImage;
	private int ballotType;
	private String fileName;
	
	public FinalBallot(Mat originalImage, int ballotType, String fileName){
		this.originalImage = originalImage;
		this.ballotType = ballotType;
		this.fileName = fileName;
	}
	
	public Mat getOriginalImage(){
		return originalImage;
	}
	
	public void setOriginalImage(Mat originalImage){
		this.originalImage = originalImage;
	}
	
	public int getBallotType(){
		return ballotType;
	}
	
	
	public void setBallotType(int ballotType){
		this.ballotType = ballotType;
	}
	
	public String getFileName(){
		return fileName;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
}
