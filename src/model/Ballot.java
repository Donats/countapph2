/**
 * A ballot object used in OCR methods.
 */
package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgcodecs.Imgcodecs;

/**
 * @author donatoamasa
 *
 */
public class Ballot {
	
	public List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	public File ballotFile;
	public Mat original;
	public Mat deskewed;
	public Mat cropped;
	public Mat grayScale;
	public Mat gaussianBlurred;
	public Mat otsuThreshold;
	public Mat otsuThresholdTwo;
	public Mat adaptiveMeanThreshold;
	public Mat adaptiveGaussianThreshold;
	public Mat adaptiveGaussianThresholdTwo;
	public Mat morphological;
	public Mat contour;
	public Mat detectedArea;
	public Mat detectedCropped;
	public Mat detectedCroppedContour;
	
	public Ballot(Mat original){
		initialize();
		this.original = original;
		setOriginalBallot(original);
	}
	
	public void initialize(){
		original = new Mat();
		deskewed = new Mat();
		cropped = new Mat();
		grayScale = new Mat();
		gaussianBlurred = new Mat();
		otsuThreshold = new Mat();
		otsuThresholdTwo = new Mat();
		adaptiveMeanThreshold = new Mat();
		adaptiveGaussianThreshold = new Mat();
		adaptiveGaussianThresholdTwo = new Mat();
		morphological = new Mat();
		contour = new Mat();
		detectedArea = new Mat();
		detectedCropped = new Mat();
		detectedCroppedContour = new Mat();
	}
	
	public File getBallotFile(){
		return ballotFile;
	}
	
	public void setOriginalBallot(Mat original){
		this.original = original;
	}
	
	public Mat getBallotSource(File ballotSource){
		return Imgcodecs.imread(ballotSource.getAbsolutePath());
	}
	
	public void setDeskewedBallot(Mat deskewed){
		this.deskewed = deskewed;
	}
	
	public void setDetectedArea(Mat detectedArea){
		this.detectedArea = detectedArea;
	}
	
	public void setDetectedCropped(Mat detectedCropped){
		this.detectedCropped = detectedCropped;
	}
	
	public void setDetectedCroppedContour(Mat detectedCroppedContour){
		this.detectedCroppedContour = detectedCroppedContour;
	}
	
	public void setCountour(Mat contour){
		this.contour = contour;
	}
	
	public Mat getOriginal(){
		return original;
	}
	
	public Mat getDeskewed(){
		return deskewed;
	}
	
	public Mat getCropped(){
		return cropped;
	}
	
	public Mat getGrayScaled(){
		return grayScale;
	}
	
	public Mat getGaussianBlurred(){
		return gaussianBlurred;
	}
	
	public Mat getOtsuThreshold(){
		return otsuThreshold;
	}
	
	public Mat getOtsuThresholdTwo(){
		return otsuThresholdTwo;
	}
	
	public Mat getAdaptiveMeanThreshold(){
		return adaptiveMeanThreshold;
	}
	
	public Mat getAdaptiveGaussianThreshold(){
		return adaptiveGaussianThreshold;
	}
	
	public Mat getAdaptiveGaussianThresholdTwo(){
		return adaptiveGaussianThresholdTwo;
	}
	
	public Mat getMorphological(){
		return morphological;
	}
	
	public Mat getContour(){
		return contour;
	}
	
	public Mat getDetectedArea(){
		return detectedArea;
	}
	
	public Mat getDetectedCrop(){
		return detectedCropped;
	}
	
	public Mat getDetectedCroppedContour(){
		return detectedCroppedContour;
	}
	
}
