/**
 * 
 */
package model;

import java.util.ArrayList;

import org.opencv.core.Point;

import model.CandidateFile.Candidate;
import model.CandidateFile.TableOfCandidates;
import model.ProcessBallot.CandidatePoints;

/**
 * @author donatoamasa
 *
 */
public class TallyVotes {
	
	public TallyVotes(ArrayList<CandidatePoints> candPoints, CandidateFile candidateFile){
		ArrayList<TableOfCandidates> tableOfCandidates = candidateFile.tableOfCandidates;
		for(int i = 0; i < tableOfCandidates.size() ;i++){
			if(tableOfCandidates.get(i).getVotes() <= tableOfCandidates.get(i).getMaxVotes()){	
				ArrayList<Candidate> candArrayList = tableOfCandidates.get(i).getCandidateArrayList();
				for(int j = 0 ; j < candArrayList.size() ; j++){
					for(int k = 0 ; k < candPoints.size() ; k++){
						int x = candArrayList.get(j).getCol();
						int y = candArrayList.get(j).getRow();
						if(candPoints.get(k).getCount() >= 5 && candPoints.get(k).getPoint().equals(new Point(x,y))){
							candArrayList.get(j).addVote();
						}
					}
				}
			}
		}
	}
}
