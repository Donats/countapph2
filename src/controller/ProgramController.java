/**
 * 
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import model.Model;
import util.Button;
import util.Lib;
import view.ClusterPanel;
import view.LoadBallotsPanel;
import view.MainFrame;
import view.MainMenuPanel;
import view.MenuPanel;
import view.ResultsPanel;
import view.MainFrame.NotificationDialog;
import view.MainFrame.PromptDialog;

/**
 * @author donatoamasa
 *
 */
public class ProgramController {
	MainFrame frame;
	Model model;	
	
	public ProgramController(Model model, MainFrame frame){
		this.model = model;
		this.frame = frame;
		new MenuController();
	}
	
	private class MenuController implements ActionListener{
		private MenuPanel menuPanel;
		private MainMenuPanel mainMenuPanel;
		private LoadBallotsPanel loadBallotsPanel;
		private ClusterPanel clusterPanel;
		private ResultsPanel resultsPanel;
		private Button about, credits, minimize, exit, home, ballots, templates, results;
		private Button openBallot, addBallots, cluster, check, save,  nextImage, prevImage, delete;
		private Button imageResult, summary, next, previous, nextTemplate, prevTemplate, accuracy;
		private JComboBox<Object> image, ballotType, rawImage, resultsBallotType;
		private JCheckBox exclude;
		private MainFrame.PromptDialog promptDialog;
		private MainFrame.ResultsDialog resultsDialog;
		
		public MenuController(){
			initialize();
			addListeners();
		}
		
		public void initialize(){
			promptDialog = PromptDialog.getInstance();
			resultsDialog = frame.resultsDialog;
			menuPanel = MenuPanel.getInstance();
			mainMenuPanel = MainMenuPanel.getInstance();
			loadBallotsPanel = LoadBallotsPanel.getInstance();
			clusterPanel = ClusterPanel.getInstance();
			resultsPanel = ResultsPanel.getInstance();
			openBallot = mainMenuPanel.getOpenBallot();
			addBallots = loadBallotsPanel.getOpenBallot();
			nextImage = loadBallotsPanel.getNext();
			prevImage = loadBallotsPanel.getPrev();
			cluster = loadBallotsPanel.getCluster();
			delete = loadBallotsPanel.getDelete();
			check = clusterPanel.getCheck();
			save = clusterPanel.getSave();
			nextTemplate = clusterPanel.getNextTemplate();
			prevTemplate = clusterPanel.getPrevTemplate();
			accuracy = clusterPanel.getAccuracy();
			about = menuPanel.getAbout();
			credits = menuPanel.getCredits();
			minimize = menuPanel.getMinimize();
			exit = menuPanel.getExit();
			home = menuPanel.getHomeButton();
			ballots = menuPanel.getBallotsButton();
			templates = menuPanel.getTemplatesButton();
			results = menuPanel.getResultsButton();
			imageResult = resultsPanel.getImageResult();
			summary = resultsPanel.getSummary();
			next = resultsPanel.getNext();
			previous = resultsPanel.getPrevious();
			image = resultsPanel.getImageComboBox();
			ballotType = resultsPanel.getBallotTypeComboBox();
			exclude = resultsPanel.getExclude();
			rawImage = loadBallotsPanel.getRawImageComboBox();
			resultsBallotType = frame.resultsDialog.getBallotTypeComboBox();
		}
		
		public void addListeners(){
			openBallot.addActionListener(this);
			addBallots.addActionListener(this);
			nextImage.addActionListener(this);
			prevImage.addActionListener(this);
			delete.addActionListener(this);
			cluster.addActionListener(this);
			check.addActionListener(this);
			save.addActionListener(this);
			about.addActionListener(this);
			credits.addActionListener(this);
			minimize.addActionListener(this);
			exit.addActionListener(this);
			home.addActionListener(this);
			ballots.addActionListener(this);
			templates.addActionListener(this);
			results.addActionListener(this);
			imageResult.addActionListener(this);
			summary.addActionListener(this);
			next.addActionListener(this);
			previous.addActionListener(this);
			accuracy.addActionListener(this);
			image.addActionListener(this);
			ballotType.addActionListener(this);
			exclude.addActionListener(this);
			rawImage.addActionListener(this);
			nextTemplate.addActionListener(this);
			prevTemplate.addActionListener(this);
			resultsBallotType.addActionListener(this);
		}
		
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == openBallot)
				gotoOpenBallot();
			else if(e.getSource() == addBallots)
				gotoOpenBallot();
			else if(e.getSource() == nextImage)
				gotoNextImage();
			else if(e.getSource() == prevImage)
				gotoPrevImage();
			else if(e.getSource() == delete)
				gotoDelete();
			else if(e.getSource() == cluster)
				gotoCluster();
			else if(e.getSource() == check)
				gotoCheck();
			else if(e.getSource() == save)
				gotoSave();
			else if(e.getSource() == imageResult)
				gotoImageResult();
			else if(e.getSource() == summary)
				gotoSummary();
			else if(e.getSource() == next)
				gotoNext();
			else if(e.getSource() == previous)
				gotoPrevious();
			else if(e.getSource() == accuracy)
				gotoAccuracy();
			else if(e.getSource() == image)
				gotoImageComboBox();
			else if(e.getSource() == ballotType)
				gotoBallotTypeComboBox();
			else if(e.getSource() == exclude)
				gotoExclude();
			else if(e.getSource() == rawImage)
				gotoRawImage();
			else if(e.getSource() == nextTemplate)
				gotoNextTemplate();
			else if(e.getSource() == prevTemplate)
				gotoPrevTemplate();
			else if(e.getSource() == resultsBallotType)
				gotoResultsBallotType();
			else if(e.getSource() == about)
				gotoAbout();
			else if(e.getSource() == credits)
				gotoCredits();
			else if(e.getSource() == minimize)
				gotoMinimize();
			else if(e.getSource() == exit)
				gotoExit();
			else if(e.getSource() == home)
				gotoHome();
			else if(e.getSource() == ballots)
				gotoBallots();
			else if(e.getSource() == templates)
				gotoTemplates();
			else if(e.getSource() == results)
				gotoResults();
		}

		private void gotoOpenBallot(){
			model.setFiles(loadBallotsPanel.getFileFromChooser());
			if(model.programState == Model.LOAD_STATE){
				loadBallotsPanel.updateRawImageComboBox(model.getFiles(), 0);
				loadBallotsPanel.showClusterButton(true);
			}
		}
		
		private void gotoNextImage(){
			loadBallotsPanel.next();
			loadBallotsPanel.updateImageLabel(model.getFiles(), rawImage.getSelectedIndex());
		}
		
		private void gotoPrevImage(){
			loadBallotsPanel.prev();
			loadBallotsPanel.updateImageLabel(model.getFiles(), rawImage.getSelectedIndex());
		}
		
		private void gotoCluster(){
			model.doClusterBallots();
		}
		
		private void gotoCheck(){
			if(model.checkTemplates())
				model.doCheckBallot();
			else
				NotificationDialog.getInstance().showNotification(NotificationDialog.SAVE_ERROR);
		}
		
		private void gotoSave(){
			Lib.saveBallotTemplate(model.getBallotTypes(), clusterPanel.getTextFields(), clusterPanel.getMaxVoteList(), clusterPanel.getIndex(), clusterPanel.getBallotTypeField().getText());
			clusterPanel.updateBallotTypeNames(model.getBallotTypes());
			model.doSave();
			NotificationDialog.getInstance().showNotification(NotificationDialog.TEMPLATE_SAVED);
		}
		
		private void gotoExclude(){
			resultsPanel.exclude();
		}

		private void gotoBallotTypeComboBox(){
			resultsPanel.updateImageComboBox(ballotType.getSelectedItem().toString());
		}

		private void gotoImageComboBox(){
			resultsPanel.imageComboBoxTriggered(image.getSelectedItem().toString(), image.getSelectedIndex());
		}
		
		private void gotoRawImage(){
			loadBallotsPanel.updateImageLabel(model.getFiles(), rawImage.getSelectedIndex());
		}

		private void gotoPrevious(){
			resultsPanel.previous();
		}

		private void gotoNext(){
			resultsPanel.next();
		}
		
		private void gotoAccuracy(){
			resultsDialog.updateTextAreaAccuracy(clusterPanel.getIndex(), model.getBallotTypes(), "OCR Accuracy");
		}

		private void gotoSummary(){
			model.doSummarize();
		}

		private void gotoImageResult(){
			model.doShowBallotInfo(image.getSelectedItem().toString());
		}
		
		private void gotoNextTemplate(){
			clusterPanel.nextTemplate(model.getBallotTypes());
		}
		
		private void gotoPrevTemplate(){
			clusterPanel.prevTemplate(model.getBallotTypes());
		}
		
		private void gotoResultsBallotType(){
			resultsDialog.updateSummaryTextArea(resultsBallotType.getSelectedItem().toString(), model.getBallotTypes());
		}
		
		private void gotoAbout(){
			promptDialog.showDialog(PromptDialog.ABOUT);
		}
		
		private void gotoCredits(){
			promptDialog.showDialog(PromptDialog.CREDITS);
		}
		
		private void gotoMinimize(){
			frame.minimizeClicked();
		}
		
		private void gotoExit(){
			frame.exitClicked();
		}
		
		private void gotoHome(){
			menuPanel.showMainMenu(true);
			model.clearFiles();
			model.clearBallotTypeContent();
		}
		
		private void gotoBallots(){
			menuPanel.showLoadBallotsPanel(true);
		}
		
		private void gotoTemplates(){
			menuPanel.showClusterPanel(true);
		}
		
		private void gotoResults(){
			menuPanel.showResultsPanel(true);
			resultsPanel.updateBallotTypeComboBox(model.getBallotTypes());
		}
		
		private void gotoDelete(){
			model.deleteBallot(rawImage.getSelectedIndex());
			int index = model.getFiles().size() == rawImage.getSelectedIndex() ? 0 : rawImage.getSelectedIndex();
			if(model.getFiles().size() > 0)
				loadBallotsPanel.updateRawImageComboBox(model.getFiles(), index);
			else
				gotoHome();
		}
	}
}
