/**
 * 
 */
package view;

import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;

import util.Lib;

/**
 * @author donatoamasa
 *
 */
public class Images {
	
	public static ImageIcon splashScreen, menuLabel, loadingDialog, resultsBackground, imageBorder;
	public static ImageIcon mainMenuIcons[][] = new ImageIcon[2][4], upperMenuIcons[][] = new ImageIcon[2][5], resultButtons[][] = new ImageIcon[2][2], clusterButtons[][] = new ImageIcon[2][2], clusterNextPrev[][] = new ImageIcon[2][2];
	public static ImageIcon dialogBackgrounds[] = new ImageIcon[2], nextprevButtons [] = new ImageIcon[3], dialogNotifications[] = new ImageIcon[6];
	
	public static void loadImages(){
		BufferedImage img;
		String dir = "/images/";
		//main menu buttons
		img = Lib.createImage(getURL(dir + "mainMenuButtons.png"));
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 3; j++)
				mainMenuIcons[i][j] = new ImageIcon(img.getSubimage(j*280, i*75, 280, 75));
		//upper menu buttons
		img = Lib.createImage(getURL(dir + "upperMenuButtons.png"));
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 5; j++)
				upperMenuIcons[i][j] = new ImageIcon(img.getSubimage(j*50, i*50, 50, 50));
		img = Lib.createImage(getURL(dir + "dialogBackgrounds.png"));
		for(int i = 0; i < 2; i++)
			dialogBackgrounds[i] = new ImageIcon(img.getSubimage(i*750, 0, 750, 500));
		img = Lib.createImage(getURL(dir + "dialogNotifications.png"));
		for(int i = 0; i < 6; i++)
			dialogNotifications[i] = new ImageIcon(img.getSubimage(i*400, 0, 400, 100));
		img = Lib.createImage(getURL(dir + "nextprevButtons.png"));
		for(int i = 0; i < 3; i++)
			nextprevButtons[i] = new ImageIcon(img.getSubimage(i*180, 0, 180, 650));
		img = Lib.createImage(getURL(dir + "label.png"));
		menuLabel = new ImageIcon(img);
		img = Lib.createImage(getURL(dir + "loadingDialog.png"));
		loadingDialog = new ImageIcon(img);
		img = Lib.createImage(getURL(dir + "resultBackground.png"));
		resultsBackground = new ImageIcon(img);
		img = Lib.createImage(getURL(dir + "imageBorder.png"));
		imageBorder = new ImageIcon(img);
		img = Lib.createImage(getURL(dir+ "resultButtons.png"));
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++)
				resultButtons[i][j] = new ImageIcon(img.getSubimage(j*280, i*50, 280, 50));
		img = Lib.createImage(getURL(dir+ "clusterButtons.png"));
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++)
				clusterButtons[i][j] = new ImageIcon(img.getSubimage(j*230, i*50, 230, 50));
		img = Lib.createImage(getURL(dir+ "clusterNextPrev.png"));
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++)
				clusterNextPrev[i][j] = new ImageIcon(img.getSubimage(j*50, i*40, 50, 40));
	}
	
	private static ImageIcon getIcon(String dir) {
		URL src = getURL(dir);
		if(src == null)
			return null;
		return new ImageIcon(src);
	}


	private static URL getURL(String dir) {
		try{
			return MainFrame.class.getResource(dir);
		}catch(Exception e){
			Lib.printMsg("Cannot load: "+dir);
		}
		return null;
	}
	
	public static void loadSplashScreen(){
		splashScreen = getIcon("/images/splashScreen.png");
	}
}
