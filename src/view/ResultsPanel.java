/**
 * 
 */
package view;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import model.BallotData;
import model.BallotType;
import model.DeskewImage;
import model.ProcessBallot;
import util.Button;
import util.Label;
import util.Lib;
import util.Panel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class ResultsPanel extends Panel{
	
	private Button imageResultButton, nextButton, previousButton, summaryButton;
	private JCheckBox exclude;
	private JComboBox<Object> ballotTypeComboBox, imageComboBox;
	private Label ballotLabel, ballotTypeLabel, imageNameLabel, resultsLabel;
	private ImageIcon icon;
	private BufferedImage bufferedImage;
	public int index;
	
	private static ResultsPanel instance;
	
	public ResultsPanel(){
		super(Lib.createRect(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT), null);
		initializeComponents();
		setComponents();
		addComponents();
		setOpaque(false);
		setVisible(false);
	}
	
	private void initializeComponents(){
		ballotLabel = new Label(null, Lib.createPoint(20, 180), Lib.createDimension(600, 660));
		imageResultButton = new Button(Images.resultButtons[0][0], Images.resultButtons[1][0], Lib.createPoint(650, 710));
		summaryButton = new Button(Images.resultButtons[0][1], Images.resultButtons[1][1], Lib.createPoint(650, 775));
		nextButton = new Button(Images.nextprevButtons[0], Images.nextprevButtons[2], Lib.createPoint(440, 175));
		previousButton = new Button(Images.nextprevButtons[0],  Images.nextprevButtons[1], Lib.createPoint(20, 175));
		ballotTypeLabel = new Label("City/Municipality:", Lib.createRect(650, 170, 280, 40), false, SwingConstants.LEFT);
		imageNameLabel = new Label("Ballot:", Lib.createRect(650, 245, 280, 40), false, SwingConstants.LEFT);
		resultsLabel = new Label("Results:", Lib.createRect(650, 670, 280, 40), false, SwingConstants.LEFT);
		ballotTypeComboBox = new JComboBox<Object>();
		imageComboBox = new JComboBox<Object>();
		exclude = new JCheckBox();
		index = 0;
	}
	
	private void setComponents(){
		
		ballotTypeLabel.setFont(Lib.getDefaultFont(16));
		ballotTypeLabel.setForeground(Lib.getDefaultColor());
		imageNameLabel.setFont(Lib.getDefaultFont(16));
		imageNameLabel.setForeground(Lib.getDefaultColor());
		resultsLabel.setFont(Lib.getDefaultFont(16));
		resultsLabel.setForeground(Lib.getDefaultColor());
		summaryButton.setToolTipText("Show the summary results");
		imageResultButton.setToolTipText("Show ballot result");
		exclude.setText("Exclude current ballot");
		exclude.setFont(Lib.getDefaultFont(16));
		exclude.setForeground(Lib.getDefaultColor());
		exclude.setHorizontalAlignment(SwingConstants.CENTER);
		exclude.setBounds(650, 330, 280, 40);
		exclude.setFocusable(false);
		exclude.setOpaque(false);
		imageComboBox.setBounds(650, 285, 280, 40);
		ballotTypeComboBox.setBounds(650, 210, 280, 40);
		ballotTypeComboBox.setFont(Lib.getDefaultFont(15));
		ballotTypeComboBox.setForeground(Lib.getDefaultColor());
		ballotTypeComboBox.setFocusable(false);
		ballotTypeComboBox.setOpaque(false);
		ballotTypeComboBox.setRenderer(Lib.getCellRenderer());
		imageComboBox.setFont(Lib.getDefaultFont(15));
		imageComboBox.setForeground(Lib.getDefaultColor());
		imageComboBox.setFocusable(false);
		imageComboBox.setOpaque(false);
		imageComboBox.setRenderer(Lib.getCellRenderer());
	}
	
	private void addComponents(){
		add(getResultsLabel());
		add(getImageResult());
		add(getSummary());
		add(getNext());
		add(getPrevious());
		add(getBallotTypeLabel());
		add(getImageNameLabel());
		add(getExclude());
		add(getBallotTypeComboBox());
		add(getImageComboBox());
		add(getBallotLabel());
	}
	
	public Label getResultsLabel(){
		return resultsLabel;
	}
	
	public Button getImageResult(){
		return imageResultButton;
	}
	
	public Button getSummary(){
		return summaryButton;
	}
	
	public Button getNext(){
		return nextButton;
	}
	
	public Button getPrevious(){
		return previousButton;
	}
	
	public JCheckBox getExclude(){
		return exclude;
	}
	
	public Label getBallotLabel(){
		return ballotLabel;
	}
	
	public Label getImageNameLabel(){
		return imageNameLabel;
	}
	
	public Label getBallotTypeLabel(){
		return ballotTypeLabel;
	}
	
	public JComboBox<Object> getBallotTypeComboBox(){
		return ballotTypeComboBox;
	}
	
	public JComboBox<Object> getImageComboBox(){
		return imageComboBox;
	}
	
	public void showNextPrevButtons(boolean flag){
		getNext().setVisible(flag);
		getPrevious().setVisible(flag);
	}
	
	public void updateBallotTypeComboBox(ArrayList<BallotType> ballotTypes){
		ArrayList<String> string = new ArrayList<String>(); 
		for(int i = 0; i < ballotTypes.size();  i++){
			if(ballotTypes.get(i).getHasContent()){
				string.add(ballotTypes.get(i).getBallotTypeName());
			}
		}
		Object [] str = (Object[]) string.toArray();
    	ballotTypeComboBox.setModel(new DefaultComboBoxModel<Object>(str));
    	updateImageComboBox(string.get(0));
	}
	
	ArrayList<String> stringNames;
	
	public void updateImageComboBox(String string){
		index = 0;
		stringNames = new ArrayList<String>(); 
		for(int i = 0; i < BallotData.getInstance().getImgName().size(); i++){
			if(BallotData.getInstance().getBallotType().get(i).equals(string))
				stringNames.add(BallotData.getInstance().getImgName().get(i));
		}
		Object [] str = (Object[]) stringNames.toArray();
    	imageComboBox.setModel(new DefaultComboBoxModel<Object>(str));
    	if(stringNames.get(0) != null)
    		updateLabel(stringNames.get(0));
    	changeCheckBox();
    	showNextPrevButtons(stringNames.size() > 1 ? true : false);
	}
	
	public void next(){
		index++;
		if(index <0 || index >= stringNames.size()){
			index = index < 0 ? stringNames.size()-1 : 0;
		}
		updateLabel(stringNames.get(index));
		imageComboBox.setSelectedIndex(index);
		changeCheckBox();
	}
	
	public void previous(){
		index--;
		if(index <0 || index >= stringNames.size()){
			index = index < 0 ? stringNames.size()-1 : 0;
		}
		updateLabel(stringNames.get(index));
		imageComboBox.setSelectedIndex(index);
		changeCheckBox();
	}
	
	public void imageComboBoxTriggered(String imageName, int indexNew){
		index = indexNew;
		updateLabel(imageName);
		changeCheckBox();
	}
	
	public void updateLabel(String imageName){
		for(int i = 0; i < BallotData.getInstance().getEditImg().size(); i++)
			if(BallotData.getInstance().getImgName().get(i).equals(imageName)){
				bufferedImage = DeskewImage.mat2Img(ProcessBallot.fitInReview(BallotData.getInstance().getEditImg().get(i)));
				icon = new ImageIcon(bufferedImage);
				ballotLabel.setIcon(icon);
				return;
			}
	}
	
	public void exclude(){
		for(int i = 0 ; i < BallotData.getInstance().getExcludeList().size() ; i++){
			if(BallotData.getInstance().getExcludeList().get(i) == stringNames.get(index)){
				BallotData.getInstance().getExcludeList().remove(i);
				return;
			}
		}
		BallotData.getInstance().getExcludeList().add(stringNames.get(index));
	}
	
	public void changeCheckBox(){
		for(int i = 0 ; i < BallotData.getInstance().getExcludeList().size() ; i++){
			if(BallotData.getInstance().getExcludeList().get(i) == stringNames.get(index)){
				exclude.setSelected(true);
				return;
			}
			else
				exclude.setSelected(false);
		}
	}
	
	public static ResultsPanel getInstance(){
		if(instance == null)
			instance = new ResultsPanel();
		return instance;
	}
}
