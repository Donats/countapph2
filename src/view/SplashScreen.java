/**
 * 
 */
package view;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JLabel;

import controller.ProgramController;
import model.Model;
import util.Lib;
import util.Window;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class SplashScreen extends Window{
	
	MainFrame frame;
	Point loc;
	JLabel label;
	
	public SplashScreen(){
		super(600, 250, null, false, new Color(0,0,0,0));
		Images.loadSplashScreen();
		initialize();
		setVisible(true);
		load();
		setVisible(false);
		frame.showFrame(true);
		
	}
	
	public void initialize(){
		label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
    		@Override
    		public void mousePressed(MouseEvent e) {
    			loc = e.getPoint();
    		}
    	});
    	label.addMouseMotionListener(new MouseMotionAdapter() {
    		@Override
    		public void mouseDragged(MouseEvent e) {
    			Point screenLoc = e.getLocationOnScreen();
    			setLocation(screenLoc.x-loc.x, screenLoc.y - loc.y);
    		}
    	});
    	label.setIcon(Images.splashScreen);
    	label.setBounds(0, 0, 600, 250);
    	
    	getContentPane().add(label);
	}
	
	private void load(){
		Images.loadImages();
		Lib.loadOpenCV();
		Lib.loadCorrectNames();
		Model model = new Model();
		frame = new MainFrame(model);
		initializeControllers(model);
	}
	
	private void initializeControllers(Model model){
		new ProgramController(model,frame);
	}
	
}
