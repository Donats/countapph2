/**
 * 
 */
package view;


import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import model.BallotType;
import model.Model;
import util.Dialog;
import util.Frame;
import util.Label;
import util.Lib;
import util.Panel;
import util.ProgressBar;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class MainFrame extends Frame{
	
	public static final int FRAMEWIDTH = 950, FRAMEHEIGHT = 850;
	public Model model;
	public static MainFrame frame;
	public PromptDialog promptDialog;
	public ResultsDialog resultsDialog;
	private static MainFrame instance;
	
	
	public MainFrame(Model model){
		super("Pinoy Ballot");
		instance = this;
		this.model = model;
		frame = this;
		init();
		Lib.setFrame(this, 3, FRAMEWIDTH, FRAMEHEIGHT, null, false, false, null);
		setUndecorated(true);
	}
	
	@Override
    public void loadImages() {
    }

    @Override
    public void initComponents() {
    	promptDialog = new PromptDialog();
    	resultsDialog = new ResultsDialog();
    }

    @Override
    public void addComponents() {
        add(MenuPanel.getInstance());
    }
    
    public static class LoadingDialog extends Dialog implements MouseMotionListener{
    	private static LoadingDialog instance;
    	private ProgressBar progressBar, progressBar2;
    	private Label processLabel, processLabel2;
		private Label background;
    	Panel loadingPanel;
		public LoadingDialog() {
			super(frame, true, FRAMEWIDTH, FRAMEHEIGHT, false, false, new Color(0,0,0,0.8f), null);
            init();
            setLocationRelativeTo(null);
            addMouseMotionListener(this);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
		}

		@Override
		public void mouseMoved(MouseEvent e) {
		}

		@Override
		public void initComponents() {
			background = new Label(Images.loadingDialog, Lib.createPoint(0, 0));
			loadingPanel = new Panel(Lib.createRect((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2, background.getWidth(), background.getHeight()), null) {};
			loadingPanel.setOpaque(false);
			processLabel = new Label("donats", Lib.createRect(0, 80, 600, 20), true, SwingConstants.CENTER);
			progressBar = new ProgressBar(new Color(51,122,183), Lib.createRect(40, 105, 520, 25));
			processLabel2 = new Label("donats", Lib.createRect(0, 20, 600, 20), true, SwingConstants.CENTER);
			progressBar2 = new ProgressBar(new Color(51,122,183), Lib.createRect(40, 45, 520, 25));
			processLabel.setFont(Lib.getDefaultFont(15));
			processLabel.setForeground(Lib.getDefaultColor());
			processLabel2.setFont(Lib.getDefaultFont(15));
			processLabel2.setForeground(Lib.getDefaultColor());
		}

		@Override
		public void addComponents() {
			loadingPanel.add(processLabel2);
			loadingPanel.add(progressBar2);
			loadingPanel.add(processLabel);
			loadingPanel.add(progressBar);
			loadingPanel.add(background);
			add(loadingPanel);
		}
		
		public void showLoadingDialog(){
			loadingPanel.setLocation((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2);
        	setVisible(true);
		}
		
		public void exitDialog(){
        	setVisible(false);
        }
		
		public void setProgressBar2Value(int value){
			progressBar2.setValue(value);
			progressBar2.repaint();
		}
		
		public void setProcessLabel2(String process){
			processLabel2.setText(process);
		}
		
		public Label getProcessLabel(){
			return processLabel;
		}
		
		public void setProgressBarValue(int value){
			progressBar.setValue(value);
			progressBar.repaint();
		}
		
		public void setProcessLabel(String process){
			processLabel.setText(process);
		}
		
		public ProgressBar getProgressBar(){
			return progressBar;
		}
		
		
		public static LoadingDialog getInstance(){
			if(instance == null)
				instance = new LoadingDialog();
			return instance;
		}
    	
    }
    
    public static class NotificationDialog extends Dialog implements MouseMotionListener{
    	private static NotificationDialog instance;
    	ImageIcon notificationType [] = new ImageIcon [6];
    	Label background;
    	Panel notificationPanel;
    	public static final int BALLOT_LOADED = 0, BALLOT_CLUSTERED = 1, TEMPLATE_SAVED = 2, BALLOT_CHECKED = 3, TEMPLATE_ERROR = 4, SAVE_ERROR = 5;
    	public int dialogIntType;
		public NotificationDialog() {
			super(frame, true, FRAMEWIDTH, FRAMEHEIGHT, false, false, new Color(0,0,0,0.8f), null);
            init();
            setLocationRelativeTo(null);
            addMouseMotionListener(this);
		}
		Point loc;
        boolean pressed;
        
        @Override
        public void mousePressed(MouseEvent e){
	         	setVisible(false);
	    }
        
        @Override
        public void mouseReleased(MouseEvent e){
            pressed = false;
        }
        
		@Override
		public void mouseDragged(MouseEvent e) {
			if(pressed)
				notificationPanel.setLocation(e.getX()-loc.x, e.getY()-loc.y);
		}

		@Override
		public void mouseMoved(MouseEvent e) {}

		@Override
		public void initComponents() {
			notificationType[BALLOT_LOADED] = Images.dialogNotifications[BALLOT_LOADED];
			notificationType[BALLOT_CLUSTERED] = Images.dialogNotifications[BALLOT_CLUSTERED];
			notificationType[TEMPLATE_SAVED] = Images.dialogNotifications[TEMPLATE_SAVED];
			notificationType[BALLOT_CHECKED] = Images.dialogNotifications[BALLOT_CHECKED];
			notificationType[TEMPLATE_ERROR] = Images.dialogNotifications[TEMPLATE_ERROR];
			notificationType[SAVE_ERROR] = Images.dialogNotifications[SAVE_ERROR];
			background = new Label(notificationType[0], Lib.createPoint(0, 0));
    		notificationPanel = new Panel(Lib.createRect((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2, background.getWidth(), background.getHeight()), null) {};
    		notificationPanel.setOpaque(false);
		}

		@Override
		public void addComponents() {
			notificationPanel.add(background);
			add(notificationPanel);
		}
		
		public void showNotification(int dialogIntType){
			this.dialogIntType = dialogIntType;
        	background.setIcon(notificationType[dialogIntType]);
        	notificationPanel.setLocation((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2);
        	setVisible(true);
		}
		
		public void exitDialog(){
        	setVisible(false);
        }
		
		public static NotificationDialog getInstance(){
			if(instance == null){
				instance = new NotificationDialog();
			}
			return instance;
		}
    	
    }
    
    public static class ResultsDialog extends Dialog implements MouseMotionListener{
    	private static ResultsDialog instance;
		private Label background, dialogName;
    	public JScrollPane resultScrollPane;
    	public JTextArea resultTextArea;
    	private JComboBox<Object> ballotTypeComboBox;
    	Panel resultsPanel;
		public ResultsDialog() {
			super(frame, true, FRAMEWIDTH, FRAMEHEIGHT, false, false, new Color(0,0,0,0.8f), null);
            init();
            setLocationRelativeTo(null);
            addMouseMotionListener(this);
		}

		Point loc;
        boolean pressed;

        @Override
        public void mousePressed(MouseEvent e){
            if(e.getX() > resultsPanel.getX() && e.getX() < resultsPanel.getX()+resultsPanel.getWidth() && e.getY() > resultsPanel.getY() && e.getY() < resultsPanel.getY()+resultsPanel.getHeight()){
               // loc = new Point(e.getX()-resultsPanel.getX(), e.getY()-resultsPanel.getY());
                pressed = true;
            }
            else
            	setVisible(false);
        }

        @Override
        public void mouseReleased(MouseEvent e){
            pressed = false;
        }
		
		@Override
		public void mouseDragged(MouseEvent e) {
		}

		@Override
		public void mouseMoved(MouseEvent e) {
		}

		@Override
		public void initComponents() {
			dialogName = new Label();
    		resultTextArea = new JTextArea();
    		resultScrollPane = new JScrollPane();
    		ballotTypeComboBox = new JComboBox<Object>();
			background = new Label(Images.resultsBackground, Lib.createPoint(0, 0));
			resultsPanel = new Panel(Lib.createRect((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2, background.getWidth(), background.getHeight()), null) {};
			resultsPanel.setOpaque(false);
			dialogName.setBounds(Lib.createRect(20, 15, 250, 50));
			dialogName.setFont(Lib.getDefaultFont(25));
			dialogName.setForeground(Lib.getDefaultColor());
			ballotTypeComboBox.setBounds(Lib.createRect(330, 20, 250, 40));
			ballotTypeComboBox.setFont(Lib.getDefaultFont(17));
			ballotTypeComboBox.setForeground(Lib.getDefaultColor());
			ballotTypeComboBox.setFocusable(false);
			ballotTypeComboBox.setOpaque(false);
			ballotTypeComboBox.setRenderer(Lib.getCellRenderer());
			resultTextArea.setColumns(20);
    		resultTextArea.setRows(5);
    		resultTextArea.setFont(Lib.getDefaultFont(20));
    		resultTextArea.setEditable(false);
    		resultScrollPane.setViewportView(resultTextArea);
    		resultScrollPane.setBounds(Lib.createRect(20, 80, 560, 700));
		}

		@Override
		public void addComponents() {
			resultsPanel.add(dialogName);
			resultsPanel.add(ballotTypeComboBox);
			resultsPanel.add(resultScrollPane);
			resultsPanel.add(background);
			add(resultsPanel);
		}
		
		public void setLabelText(String labelText){
			dialogName.setText("" + labelText);
		}
		
		public void showResultsDialog(boolean flag){
			ballotTypeComboBox.setVisible(flag);
			resultsPanel.setLocation((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2);
        	setLabelText(flag ? "Summary Result" : "Ballot Result");
			setVisible(true);
		}
		
		public void updateBallotTypeComboBox(ArrayList<BallotType> ballotTypes){
			ArrayList<String> string = new ArrayList<String>(); 
			for(int i = 0; i < ballotTypes.size(); string.add(ballotTypes.get(i).getBallotTypeName()), i++);
			Object [] str = (Object[]) string.toArray();
	    	ballotTypeComboBox.setModel(new DefaultComboBoxModel<Object>(str));
	    	updateSummaryTextArea(ballotTypes.get(0).getBallotTypeName(), ballotTypes);
		}
		
		public void exitDialog(){
        	setVisible(false);
        }
		
		public void updateTextArea(String result){
			resultTextArea.setText("" + result);
			resultTextArea.setCaretPosition(0);
        }
		
		public void updateTextAreaAccuracy(int index, ArrayList<BallotType> ballotTypes, String labelText){
			showResultsDialog(false);
			resultTextArea.setText(""+ballotTypes.get(index).getNamesAccuracy());
			resultTextArea.setCaretPosition(0);
			setLabelText(labelText);
		}
		
		public void updateSummaryTextArea(String ballotTypeName, ArrayList<BallotType> ballotTypes){
			resultTextArea.setText("");
			for(int i = 0; i < ballotTypes.size(); i++)
				if(ballotTypes.get(i).getBallotTypeName().equals(ballotTypeName)){
					updateTextArea(Lib.readResult(ballotTypes.get(i).getSummaryResultPath()));
					return;
				}
		}
		
		public JComboBox<Object> getBallotTypeComboBox(){
			return ballotTypeComboBox;
		}
		
		public static ResultsDialog getInstance(){
			if(instance == null)
				instance = new ResultsDialog();
			return instance;
		}
    	
    }
    
    public static class PromptDialog extends Dialog implements MouseMotionListener{
    	Label background;
    	ImageIcon dialogType[] = new ImageIcon[2];
    	Panel dialogPanel;
    	public int dialogIntType;
    	public static final int ABOUT = 0, CREDITS = 1;
    	private static PromptDialog instance;
    	
    	public PromptDialog(){
    		super(frame, true, FRAMEWIDTH, FRAMEHEIGHT, false, false, new Color(0,0,0,0.5f), null);
            init();
            setLocationRelativeTo(null);
            addMouseMotionListener(this);
    	}
    	
		@Override
        public void initComponents() {
    		dialogType[ABOUT] = Images.dialogBackgrounds[ABOUT];
    		dialogType[CREDITS] = Images.dialogBackgrounds[CREDITS];
    		background = new Label(dialogType[0], Lib.createPoint(0, 0));
    		dialogPanel = new Panel(Lib.createRect((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2, background.getWidth(), background.getHeight()), null) {};
    		dialogPanel.setOpaque(false);
    	}

        @Override
        public void addComponents() {
            dialogPanel.add(background);
            add(dialogPanel);
        }

        Point loc;
        boolean pressed;

        @Override
        public void mousePressed(MouseEvent e){
            if(e.getX() > dialogPanel.getX() && e.getX() < dialogPanel.getX()+dialogPanel.getWidth() && e.getY() > dialogPanel.getY() && e.getY() < dialogPanel.getY()+dialogPanel.getHeight()){
                loc = new Point(e.getX()-dialogPanel.getX(), e.getY()-dialogPanel.getY());
                pressed = true;
            }
            else
            	setVisible(false);
        }

        @Override
        public void mouseReleased(MouseEvent e){
            pressed = false;
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if(pressed)
            	dialogPanel.setLocation(e.getX()-loc.x, e.getY()-loc.y);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }
        
        public void showDialog(int dialogIntType){
        	this.dialogIntType = dialogIntType;
        	background.setIcon(dialogType[dialogIntType]);
        	dialogPanel.setLocation((MainFrame.FRAMEWIDTH-background.getWidth())/2, (MainFrame.FRAMEHEIGHT-background.getHeight())/2);
        	setVisible(true);
        }
        
        public void exitDialog(){
        	setVisible(false);
        }
        
        public static PromptDialog getInstance(){
        	if(instance == null)
        		instance = new PromptDialog();
        	return instance;
        }
    }
    
    public void minimizeClicked(){
    	setExtendedState(JFrame.ICONIFIED);
    }
    
    public void exitClicked(){
    	System.exit(0);
    }
    public static MainFrame getInstance() {
		return instance;
	}
}
