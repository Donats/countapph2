/**
 * 
 */
package view;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import util.Button;
import util.Label;
import util.Lib;
import util.PanelNew;
import util.TabbedRect;
import view.MainFrame.LoadingDialog;
import view.MainFrame.NotificationDialog;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class MenuPanel extends PanelNew{

	public static final int HOME = 0, BALLOTS = 1, TEMPLATES = 2, RESULTS = 3;
	private Button aboutButton, creditsButton, minimizeButton, exitButton;
	private Label home, ballots, templates, results;
	private Button homeButton, ballotsButton, templatesButton, resultsButton;
	private static MenuPanel instance;
	private JLabel backgroundImage;
	private TabbedRect tabbedRect;
	
	public MenuPanel(){
		super(Lib.createRect(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT), null);
		initComponents();
		addComponents();
	}
	@Override
	public void initComponents() {
		home = new Label("Home", Lib.createRect(0, 90, 150, 60), false, SwingConstants.CENTER);
		ballots = new Label("Ballots", Lib.createRect(150, 90, 150, 60), false, SwingConstants.CENTER);
		templates = new Label("Templates", Lib.createRect(300, 90, 150, 60), false, SwingConstants.CENTER);
		results = new Label("Results", Lib.createRect(450, 90, 150, 60), false, SwingConstants.CENTER);
		
		homeButton = new Button(Lib.createRect(0, 93, 150, 60));
		ballotsButton = new Button(Lib.createRect(150, 93, 150, 60));
		templatesButton = new Button(Lib.createRect(300, 93, 150, 60));
		resultsButton = new Button(Lib.createRect(450, 93, 150, 60));
		
		aboutButton = new Button(Images.upperMenuIcons[0][0], Images.upperMenuIcons[1][0], Lib.createPoint(690, 10));
		creditsButton = new Button(Images.upperMenuIcons[0][1], Images.upperMenuIcons[1][1], Lib.createPoint(755, 10));
		minimizeButton = new Button(Images.upperMenuIcons[0][2], Images.upperMenuIcons[1][2], Lib.createPoint(820, 10));
		exitButton = new Button(Images.upperMenuIcons[0][3], Images.upperMenuIcons[1][3], Lib.createPoint(885, 10));
		
		backgroundImage = new JLabel("");
		backgroundImage.setIcon(new ImageIcon(MainMenuPanel.class.getResource("/images/background.png")));
		
		tabbedRect = new TabbedRect(new Color(255,255,255), Lib.createRect(0, 136, 150, 7));
		
		backgroundImage.setBounds(0, 0, 950, 850);

		home.setFont(Lib.getDefaultFont(20));
		home.setForeground(new Color(199,232,247));
		ballots.setFont(Lib.getDefaultFont(20));
		ballots.setForeground(new Color(199,232,247));
		templates.setFont(Lib.getDefaultFont(20));
		templates.setForeground(new Color(199,232,247));
		results.setFont(Lib.getDefaultFont(20));
		results.setForeground(new Color(199,232,247));
		
		templatesButton.setEnabled(false);
		ballotsButton.setEnabled(false);
		resultsButton.setEnabled(false);
		showMainMenu(true);
	}
	
	@Override
	public void addComponents() {
		add(getTabbedRect());
		add(getHomeButton());
		add(getBallotsButton());
		add(getTemplatesButton());
		add(getResultsButton());
		add(getHome());
		add(getBallots());
		add(getTemplates());
		add(getResults());
		add(getAbout());
		add(getCredits());
		add(getMinimize());
		add(getExit());
		add(MainMenuPanel.getInstance());
		add(LoadBallotsPanel.getInstance());
		add(ClusterPanel.getInstance());
		add(ResultsPanel.getInstance());
		add(backgroundImage);
	}
	
	public TabbedRect getTabbedRect(){
		return tabbedRect;
	}
	
	public Button getHomeButton(){
		return homeButton;
	}
	
	public Button getBallotsButton(){
		return ballotsButton;
	}
	
	public Button getTemplatesButton(){
		return templatesButton;
	}
	
	public Button getResultsButton(){
		return resultsButton;
	}
	
	public Label getHome(){
		return home;
	}
	
	public Label getBallots(){
		return ballots;
	}
	
	public Label getTemplates(){
		return templates;
	}
	
	public Label getResults(){
		return results;
	}
	
	public Button getAbout(){
		return aboutButton;
	}
	
	public Button getCredits(){
		return creditsButton;
	}
	
	public Button getMinimize(){
		return minimizeButton;
	}
	
	public Button getExit(){
		return exitButton;
	}
	
	public void setTabbedButtons(int index){
		home.setForeground(index==HOME? new Color(255,255,255) : new Color(199,232,247));
		ballots.setForeground(index==BALLOTS? new Color(255,255,255) : new Color(199,232,247));
		templates.setForeground(index==TEMPLATES? new Color(255,255,255) : new Color(199,232,247));
		results.setForeground(index==RESULTS? new Color(255,255,255) : new Color(199,232,247));
		resultsButton.setEnabled(hasChecked ? true : false);
		ballotsButton.setEnabled(hasFiles ? true : false); 
		tabbedRect.setBounds(index*150, 143, 150, 7);
	}
	
	public boolean hasChecked = false, hasFiles = false;
	
	public void showMainMenu(boolean flag){
		this.hasChecked = !flag;
		this.hasFiles = !flag;
		MainMenuPanel.getInstance().setVisible(flag);
		LoadBallotsPanel.getInstance().setVisible(!flag);
		ResultsPanel.getInstance().setVisible(!flag);
		ClusterPanel.getInstance().showCheckButton(!flag);
		ClusterPanel.getInstance().setVisible(!flag);
		setTabbedButtons(HOME);
	}
	
	public void showLoadBallotsPanel(boolean flag){
		this.hasFiles = flag;
		LoadBallotsPanel.getInstance().setVisible(flag);
		MainMenuPanel.getInstance().setVisible(!flag);
		ResultsPanel.getInstance().setVisible(!flag);
		ClusterPanel.getInstance().setVisible(!flag);
		ballotsButton.setEnabled(true);
		setTabbedButtons(BALLOTS);
	}
	
	public void showClusterPanel(boolean flag){
		ClusterPanel.getInstance().setVisible(flag);
		MainMenuPanel.getInstance().setVisible(!flag);
		LoadBallotsPanel.getInstance().setVisible(!flag);
		ResultsPanel.getInstance().setVisible(!flag);
		templatesButton.setEnabled(true);
		setTabbedButtons(TEMPLATES);
	}
	
	public void showResultsPanel(boolean flag){
		this.hasChecked = flag;
		ResultsPanel.getInstance().setVisible(flag);
		ClusterPanel.getInstance().setVisible(!flag);
		LoadBallotsPanel.getInstance().setVisible(!flag);
		setTabbedButtons(RESULTS);
	}
	
	public void beforeClustering(){
		LoadingDialog.getInstance().showLoadingDialog();
		LoadingDialog.getInstance().setProcessLabel2(Lib.CLUSTER_IMAGE);
		LoadingDialog.getInstance().setProcessLabel(Lib.CLUSTER_IMAGE);
		LoadingDialog.getInstance().setProgressBar2Value(0);
		LoadingDialog.getInstance().setProgressBarValue(0);
	}
	
	public void afterClustering(){
		LoadingDialog.getInstance().exitDialog();
		SwingUtilities.invokeLater(new Runnable() {
		     public void run() {
		    	 NotificationDialog.getInstance().showNotification(NotificationDialog.BALLOT_CLUSTERED);
		     }
		});
		ClusterPanel.getInstance().showCheckButton(true);
		LoadBallotsPanel.getInstance().showClusterButton(false);
		showClusterPanel(true);
	}
	
	public void errorOccured(){
		LoadingDialog.getInstance().exitDialog();
		SwingUtilities.invokeLater(new Runnable() {
		     public void run() {
		    	 NotificationDialog.getInstance().showNotification(NotificationDialog.ERROR);
		     }
		});
		showClusterPanel(true);
	}
	
	public void beforeCheckingAndCounting(){
		LoadingDialog.getInstance().showLoadingDialog();
		LoadingDialog.getInstance().setProgressBar2Value(0);
		LoadingDialog.getInstance().setProgressBarValue(0);
		LoadingDialog.getInstance().setProcessLabel(Lib.CHECK_BALLOT);
	}
	
	public void afterCheckingAndCounting(){
		LoadingDialog.getInstance().exitDialog();
		SwingUtilities.invokeLater(new Runnable() {
		     public void run() {
		    	 NotificationDialog.getInstance().showNotification(NotificationDialog.BALLOT_CHECKED);
		     }
		});
		ClusterPanel.getInstance().showCheckButton(false);
		LoadBallotsPanel.getInstance().showClusterButton(false);
		showResultsPanel(true);
	}
	
	public static MenuPanel getInstance(){
		if(instance == null)
			instance = new MenuPanel();
		return instance;
	}

}
