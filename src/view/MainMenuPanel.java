/**
 * 
 */
package view;


import javax.swing.ImageIcon;

import util.Button;
import util.Label;
import util.Lib;
import util.Panel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class MainMenuPanel extends Panel{
	private Button openBallotButton;
	private Label mainLabel;
	private static MainMenuPanel instance;
	
	public MainMenuPanel(){
		super(Lib.createRect(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT), null);
		initializeComponents();
		setComponents();
		addComponents();
		setOpaque(false);
	}
	
	private void initializeComponents(){
		mainLabel = new Label( Images.menuLabel, Lib.createPoint(50, 200));
		openBallotButton = new Button(Images.mainMenuIcons[0][0], Images.mainMenuIcons[1][0], Lib.createPoint(650, 750));
	}
	
	public void setComponents(){
		openBallotButton.setToolTipText("Open selected ballots");
		openBallotButton.setDisabledIcon(Images.mainMenuIcons[0][0]);
	}
	
	public void addComponents(){
		add(getOpenBallot());
		add(getMainLabel());
	}
	
	public void showMainMenuOptions(boolean flag){
		setVisible(flag);
	}
	
	public void setBallotLabel(ImageIcon ballotSource){
		mainLabel.setBounds(115, 75, 350, 420);
		mainLabel.setIcon(ballotSource);
	}
	
	public Button getOpenBallot(){
		return openBallotButton;
	}
	
	public Label getMainLabel(){
		return mainLabel;
	}

	public static MainMenuPanel getInstance(){
		if(instance == null)
			instance = new MainMenuPanel();
		return instance;
	}
}
