/**
 * 
 */
package view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import model.DeskewImage;
import util.Button;
import util.Label;
import util.Lib;
import util.Panel;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class LoadBallotsPanel extends Panel{

	private Button openBallotButton, clusterButton, next, prev, deleteBallot;
	private JComboBox<Object> rawImageComboBox;
	private Label imageLabel, imageNameLabel, imageBorder;
	private ImageIcon icon;
	private BufferedImage bufferedImage;
	private static LoadBallotsPanel instance;
	private JFileChooser openBallotChooser;
	private int returnValOpenChooser;
	private FileNameExtensionFilter ballotFilter;
	
	public LoadBallotsPanel(){
		super(Lib.createRect(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT), null);
		initializeComponents();
		setComponents();
		addComponents();
		setOpaque(false);
	}
	
	public void initializeComponents(){
		next = new Button(Images.nextprevButtons[0], Images.nextprevButtons[2], Lib.createPoint(390, 180));
		prev = new Button(Images.nextprevButtons[0], Images.nextprevButtons[1], Lib.createPoint(40, 180));
		
		deleteBallot = new Button(Images.upperMenuIcons[0][4], Images.upperMenuIcons[1][4], Lib.createPoint(880, 210));
		
		openBallotButton = new Button(Images.mainMenuIcons[0][0], Images.mainMenuIcons[1][0], Lib.createPoint(650, 750));
		clusterButton = new Button(Images.mainMenuIcons[0][1], Images.mainMenuIcons[1][1], Lib.createPoint(650, 660));
		
		imageNameLabel = new Label("Ballot:", Lib.createRect(650, 170, 280, 40), false, SwingConstants.LEFT);
		imageLabel = new Label(null, Lib.createPoint(105, 180), Lib.createDimension(400, 650));
		imageBorder = new Label(Images.imageBorder, Lib.createPoint(85, 165), Lib.createDimension(440, 680));
		rawImageComboBox = new JComboBox<Object>();
		
		openBallotChooser = new JFileChooser();
		ballotFilter = new FileNameExtensionFilter("Image Files", new String [] {"jpg", "png", "jpeg", "bmp", "tif"});
	}
	
	public void setComponents(){
		
		imageNameLabel.setFont(Lib.getDefaultFont(16));
		imageNameLabel.setForeground(Lib.getDefaultColor());
		
		openBallotButton.setToolTipText("Add more ballots");
		openBallotButton.setDisabledIcon(Images.mainMenuIcons[0][0]);
		
		clusterButton.setToolTipText("Cluster selected ballot");
		clusterButton.setDisabledIcon(Images.mainMenuIcons[0][1]);
		
		deleteBallot.setToolTipText("Delete Ballot");
		
		rawImageComboBox.setBounds(650, 215, 220, 40);
		rawImageComboBox.setFont(Lib.getDefaultFont(15));
		rawImageComboBox.setForeground(Lib.getDefaultColor());
		rawImageComboBox.setFocusable(false);
		rawImageComboBox.setOpaque(false);
		rawImageComboBox.setRenderer(Lib.getCellRenderer());
		
		openBallotChooser.setMultiSelectionEnabled(true);
		openBallotChooser.setDialogTitle("Select ballot samples");
		openBallotChooser.addChoosableFileFilter(ballotFilter);
		openBallotChooser.setAcceptAllFileFilterUsed(false);
	}
	
	public void addComponents(){
		add(getDelete());
		add(getNext());
		add(getPrev());
		add(getOpenBallot());
		add(getCluster());
		add(getImageNameLabel());
		add(getImageLabel());
		add(getImageBorder());
		add(getRawImageComboBox());
	}
	
	public Button getOpenBallot(){
		return openBallotButton;
	}
	
	public Button getCluster(){
		return clusterButton;
	}

	public Label getImageNameLabel(){
		return imageNameLabel;
	}
	
	public Label getImageLabel(){
		return imageLabel;
	}
	
	public Label getImageBorder(){
		return imageBorder;
	}
	
	public JComboBox<Object> getRawImageComboBox(){
		return rawImageComboBox;
	}
	
	public Button getNext(){
		return next;
	}
	
	public Button getPrev(){
		return prev;
	}
	
	public Button getDelete(){
		return deleteBallot;
	}
	
	public void showClusterButton(boolean flag){
		clusterButton.setVisible(flag);
	}
	
	public File[] getFileFromChooser(){
		returnValOpenChooser = openBallotChooser.showOpenDialog(MainFrame.getInstance());
		return (returnValOpenChooser == JFileChooser.APPROVE_OPTION ? openBallotChooser.getSelectedFiles() : null);
	}
	
	public void showNextPrevButtons(boolean flag){
		getNext().setVisible(flag);
		getPrev().setVisible(flag);
	}
	
	public int index = 0; 
	
	ArrayList<String> string;
	public void updateRawImageComboBox(ArrayList<File> allFiles, int index){
		this.index = index;
		string = new ArrayList<String>(); 
		for(int i = 0; i < allFiles.size(); string.add(allFiles.get(i).getName()), i++);
		Object [] str = (Object[]) string.toArray();
		rawImageComboBox.setModel(new DefaultComboBoxModel<Object>(str));
		updateImageLabel(allFiles, index);
		rawImageComboBox.setSelectedIndex(index);
		showNextPrevButtons(string.size() > 1 ? true : false);
	}
	
	public void next(){
		index++;
		if(index <0 || index >= string.size()){
			index = index < 0 ? string.size()-1 : 0;
		}
		rawImageComboBox.setSelectedIndex(index);
	}
	
	public void prev(){
		index--;
		if(index <0 || index >= string.size()){
			index = index < 0 ? string.size()-1 : 0;
		}
		rawImageComboBox.setSelectedIndex(index);
	}
	
	public void updateImageLabel(ArrayList<File> allFiles, int index){
		this.index = index;
		Mat sourceImage =  Imgcodecs.imread(allFiles.get(index).getAbsolutePath());
		bufferedImage = DeskewImage.mat2Img(Lib.showInPanel(sourceImage));
		icon = new ImageIcon(bufferedImage);
		imageLabel.setIcon(icon);
		return;
	}
	
	public static LoadBallotsPanel getInstance(){
		if(instance == null)
			instance = new LoadBallotsPanel();
		return instance;
	}
}
