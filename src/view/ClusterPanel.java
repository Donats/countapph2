/**
 * 
 */
package view;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import model.BallotType;
import model.CandidateName;
import util.Button;
import util.Label;
import util.Lib;
import util.Panel;
import util.StatusCircle;

/**
 * @author donatoamasa
 *
 */
@SuppressWarnings("serial")
public class ClusterPanel extends Panel{
	public ArrayList<JTextField> textFieldList = new ArrayList<JTextField>();
	public ArrayList<JTextField> maxVotesList = new ArrayList<JTextField>();
	private static ClusterPanel instance;
	private Button checkButton, saveButton, nextTemplate, prevTemplate, accuracy;
	private JScrollPane templateScrollPane;
	private JTextField ballotTypeField;
	private Label templateLabel, status, templateStatus;
	private StatusCircle statusCircle;
	private int index;
	
	public ClusterPanel(){
		super(Lib.createRect(0, 0, MainFrame.FRAMEWIDTH, MainFrame.FRAMEHEIGHT), null);
		initializeComponents();
		setComponents();
		addComponents();
		setOpaque(false);
		setVisible(false);
	}
	
	private void initializeComponents(){
		index = 0;
		templateScrollPane = new JScrollPane();
		ballotTypeField = new JTextField();
		templateLabel = new Label("City/Municipality:", Lib.createRect(340, 730, 230, 30), false, SwingConstants.LEFT);
		status =  new Label("Status:", Lib.createRect(340, 805, 230, 30), false, SwingConstants.LEFT);
		templateStatus =  new Label("Unsaved", Lib.createRect(320, 805, 230, 30), false, SwingConstants.RIGHT);
		accuracy = new Button(Images.clusterButtons[0][1], Images.clusterButtons[1][1], Lib.createPoint(20, 790));
		nextTemplate = new Button(Images.clusterNextPrev[0][1], Images.clusterNextPrev[1][1], Lib.createPoint(570, 760));
		prevTemplate = new Button(Images.clusterNextPrev[0][0], Images.clusterNextPrev[1][0], Lib.createPoint(290, 760));
		checkButton = new Button(Images.mainMenuIcons[0][2], Images.mainMenuIcons[1][2], Lib.createPoint(650, 750));
		saveButton = new Button(Images.clusterButtons[0][0], Images.clusterButtons[1][0], Lib.createPoint(20, 735));
		statusCircle = new StatusCircle(new Color(165,165,165), Lib.createRect(545, 805, 30, 30));
	}
	
	private void setComponents(){
		
		templateLabel.setFont(Lib.getDefaultFont(16));
		templateLabel.setForeground(Lib.getDefaultColor());
		
		status.setFont(Lib.getDefaultFont(16));
		status.setForeground(Lib.getDefaultColor());
		
		templateStatus.setFont(Lib.getDefaultFont(16));
		templateStatus.setForeground(Lib.getDefaultColor());
		
		checkButton.setToolTipText("Check the ballots");
		checkButton.setDisabledIcon(Images.mainMenuIcons[0][2]);
		 
		saveButton.setToolTipText("Save template");
		saveButton.setDisabledIcon(Images.mainMenuIcons[0][3]);
		
		ballotTypeField.setBounds(Lib.createRect(340, 760, 230, 40));
		ballotTypeField.setFont(Lib.getDefaultFont(16));
		ballotTypeField.setForeground(Lib.getDefaultColor());
		ballotTypeField.setHorizontalAlignment(SwingConstants.CENTER);
		
		templateScrollPane.setBounds(20, 170, 905, 550);
		templateScrollPane.setFocusable(false);
	}
	
	private void addComponents(){
		add(getStatusCircle());
		add(getTemplateStatus());
		add(getStatus());
		add(getTemplateLabel());
		add(getAccuracy());
		add(getNextTemplate());
		add(getPrevTemplate());
		add(getBallotTypeField());
		add(getTemplateScrollPane());
		add(getCheck());
		add(getSave());
	}
	
	public Label getTemplateStatus(){
		return templateStatus;
	}
	
	public StatusCircle getStatusCircle(){
		return statusCircle;
	}
	
	public Label getStatus(){
		return status;
	}
	
	public Label getTemplateLabel(){
		return templateLabel;	
	}
	
	public Button getAccuracy(){
		return accuracy;
	}
	
	public Button getNextTemplate(){
		return nextTemplate;
	}
	
	public Button getPrevTemplate(){
		return prevTemplate;
	}
	
	public JTextField getBallotTypeField(){
		return ballotTypeField;
	}
	
	public JScrollPane getTemplateScrollPane(){
		return templateScrollPane;
	}
	
	public Button getCheck(){
		return checkButton;
	}
	
	public Button getSave(){
		return saveButton;
	}
	
	public int getIndex(){
		return index;
	}
	
	public ArrayList<JTextField> getTextFields(){
		return textFieldList;
	}
	
	public ArrayList<JTextField> getMaxVoteList(){
		return maxVotesList;
	}
	
	public void showCheckButton(boolean flag){
		checkButton.setVisible(flag);
	}
	
	public void enableNextPrevButtons(boolean flag){
		getNextTemplate().setVisible(flag);
		getPrevTemplate().setVisible(flag);
	}
	
	ArrayList<String> stringNames;
	
	public void updateBallotTypeNames(ArrayList<BallotType> ballotTypes){
		stringNames = new ArrayList<String>();
		for(int i = 0; i < ballotTypes.size(); stringNames.add(ballotTypes.get(i).getBallotTypeName()), i++);
		updateTextField(stringNames.get(index));
		enableNextPrevButtons(stringNames.size() > 1 ? true : false);
		createPanel(index, ballotTypes);
		checkSaved(ballotTypes.get(index).getIsSaved());
	}
	
	public void checkSaved(boolean isSaved){
		templateStatus.setText(isSaved ? "Saved" : "Unsaved");
		statusCircle.setColor(isSaved ? new Color(74,193,2) : new Color(165,165,165));
	}
	
	public void createPanel(int index, ArrayList<BallotType> ballotTypes){
		textFieldList.clear();
		maxVotesList.clear();
		ArrayList<CandidateName> candidateNames = ballotTypes.get(index).getCandidateNames();
		JPanel panel = new JPanel(); int rowCounter = 0, columnCounter = 0;
		panel.setLayout(null);
		for(int i = 0; i < candidateNames.size(); i++, columnCounter++){
			if(candidateNames.get(i).getNewRow() && columnCounter<4 && i!= 0){
				for(int j = columnCounter; j < 4; j++){
					JTextField textFieldNull = new JTextField();
					setTextField(textFieldNull, Lib.createRect(j*220, rowCounter*30, 220, 30), true);
					textFieldNull.setEditable(false);
					panel.add(textFieldNull);
				}
			}
			columnCounter = candidateNames.get(i).getNewRow() ? 0 : columnCounter == 4 ? 0 : columnCounter;
			if(candidateNames.get(i).getNewSet()){
				rowCounter = i==0? rowCounter: rowCounter+1;
				JLabel label = new JLabel(candidateNames.get(i).getCandidatePosition() + " / Vote for " + 
						(Lib.checkMaxVotes(candidateNames.get(i).getCandidatePosition()) ? "" : candidateNames.get(i).getMaxVote()));
				setLabel(label, Lib.createRect(0, rowCounter*30, 880, 30), candidateNames.get(i).getCandidatePosition());
				if(Lib.checkMaxVotes(candidateNames.get(i).getCandidatePosition())){
					JTextField maxVoteField = new JTextField(candidateNames.get(i).getMaxVote());
					setTextField(maxVoteField, Lib.createRect(candidateNames.get(i).getCandidatePosition().equals("Member, Sangguniang Panlalawigan") ? 610 : 630, rowCounter*30, 30, 30), true);
					maxVotesList.add(maxVoteField);
					panel.add(maxVoteField);
				}
				columnCounter = 0;
				rowCounter++;
				panel.add(label);
			}
			else if(candidateNames.get(i).getNewRow()){
				rowCounter++;
			}
			JTextField textField = new JTextField(candidateNames.get(i).getCandidateName());
			setTextField(textField, Lib.createRect(columnCounter*220, rowCounter*30, 220, 30), candidateNames.get(i).getIsNameCorrect());
			panel.add(textField);
			textFieldList.add(textField);
		}
		panel.setPreferredSize(Lib.createDimension(880, textFieldList.get(textFieldList.size()-1).getY()+textFieldList.get(textFieldList.size()-1).getHeight()));
		templateScrollPane.setViewportView(panel);
		System.gc();
	}
	
	public void setTextField(JTextField textfield, Rectangle rect, boolean isCorrect){
		textfield.setBounds(rect);
		textfield.setFont(Lib.getDefaultFont(14));
		textfield.setForeground(isCorrect ? Lib.getDefaultColor() : Color.RED);
		textfield.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	public void setLabel(JLabel label, Rectangle rect, String position){
		label.setOpaque(true);
		label.setBounds(rect);
		label.setFont(Lib.getDefaultFont(16));
		label.setForeground(Color.WHITE);
		label.setBackground(Lib.getPositionBackgroundColor(position));
		label.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	public void updateTextField(String ballotTypeName){
		ballotTypeField.setText(ballotTypeName);
	}
	
	public void nextTemplate(ArrayList<BallotType> ballotTypes){
		index++;
		if(index <0 || index >= stringNames.size()){
			index = index < 0 ? stringNames.size()-1 : 0;
		}
		updateTextField(stringNames.get(index));
		createPanel(index, ballotTypes);
		checkSaved(ballotTypes.get(index).getIsSaved());
	}
	
	public void prevTemplate(ArrayList<BallotType> ballotTypes){
		index--;
		if(index <0 || index >= stringNames.size()){
			index = index < 0 ? stringNames.size()-1 : 0;
		}
		updateTextField(stringNames.get(index));
		createPanel(index, ballotTypes);
		checkSaved(ballotTypes.get(index).getIsSaved());
	}

	public static ClusterPanel getInstance(){
		if(instance == null)
			instance = new ClusterPanel();
		return instance;
	}
}
